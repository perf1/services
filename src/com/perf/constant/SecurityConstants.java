package com.perf.constant;

public class SecurityConstants {
	public static final String AUTHORIZATION = "Authorization";
	public static final String JWT_BEARER_TOKEN_PREFIX = "Bearer ";
	public static final int BEARER_PREFIX_LENGTH = JWT_BEARER_TOKEN_PREFIX.length();
	public static final String AUTHORITIES = "authorities";
	public static final String USER = "user";
	public static final String USER_ID = "userId";
	public static final String ROLE_REGISTERED_USER = "ROLE_REGISTERED_USER";
	public static final String ROLE_ADMIN= "ROLE_ADMIN";

}
