package com.perf.dto.order;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class OrdersDTO {

	private Long ordersId;
	private BigDecimal totalProduct;
	private BigDecimal totalTax;
	private Timestamp timePlaced;
	private Timestamp lastUpdate;
	private String status;
	private Long billingAddressId;
	public Long getOrdersId() {
		return ordersId;
	}
	public void setOrdersId(Long ordersId) {
		this.ordersId = ordersId;
	}
	public BigDecimal getTotalProduct() {
		return totalProduct;
	}
	public void setTotalProduct(BigDecimal totalProduct) {
		this.totalProduct = totalProduct;
	}
	public BigDecimal getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}
	public Timestamp getTimePlaced() {
		return timePlaced;
	}
	public void setTimePlaced(Timestamp timePlaced) {
		this.timePlaced = timePlaced;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getBillingAddressId() {
		return billingAddressId;
	}
	public void setBillingAddressId(Long billingAddressId) {
		this.billingAddressId = billingAddressId;
	}
	
	
}
