package com.perf.dto.items;

public class AttributeDTO {
	Long attributeId;
	Long attributeValueId;
	String name;
	String description;
	String value;
	public Long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}
	public Long getAttributeValueId() {
		return attributeValueId;
	}
	public void setAttributeValueId(Long attributeValId) {
		this.attributeValueId = attributeValId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
