package com.perf.dto.items;

import java.sql.Timestamp;
import java.util.Collection;

public class MenuItemDTO {
	 
	private Long menuItemId;
	 
	private String identifier;
	private String menuItemName;
	 
	private String menuItemDesc;
	private Timestamp creationTime ;
	private Timestamp lastUpdate ;
	 	 
	Collection<CategoryDTO> categories; 
	Collection<AttributeDTO> attributes;
	public Long getMenuItemId() {
		return menuItemId;
	}
	public void setMenuItemId(Long menuItemId) {
		this.menuItemId = menuItemId;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getMenuItemName() {
		return menuItemName;
	}
	public void setMenuItemName(String menuItemName) {
		this.menuItemName = menuItemName;
	}
	public String getMenuItemDesc() {
		return menuItemDesc;
	}
	public void setMenuItemDesc(String menuItemDesc) {
		this.menuItemDesc = menuItemDesc;
	}
	public Timestamp getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Collection<CategoryDTO> getCategories() {
		return categories;
	}
	public void setCategories(Collection<CategoryDTO> categories) {
		this.categories = categories;
	}
	public Collection<AttributeDTO> getAttributes() {
		return attributes;
	}
	public void setAttributes(Collection<AttributeDTO> attributes) {
		this.attributes = attributes;
	}

}
