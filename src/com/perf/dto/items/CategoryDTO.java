package com.perf.dto.items;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class CategoryDTO {
Long categoryId;
	
	String name;
	String description; 
	boolean isTopCategory =false; 
	Collection<CategoryDTO> parentCategories; 
	Collection<CategoryDTO> childCategories; 
	Collection<MenuItemDTO> menuItems;
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isTopCategory() {
		return isTopCategory;
	}
	public void setTopCategory(boolean isTopCategory) {
		this.isTopCategory = isTopCategory;
	}
	public Collection<CategoryDTO> getParentCategories() {
		return parentCategories;
	}
	public void setParentCategories(Collection<CategoryDTO> parentCategories) {
		this.parentCategories = parentCategories;
	}
	public Collection<CategoryDTO> getChildCategories() {
		return childCategories;
	}
	public void setChildCategories(Collection<CategoryDTO> childCategories) {
		this.childCategories = childCategories;
	}
	public Collection<MenuItemDTO> getMenuItems() {
		return menuItems;
	}
	public void setMenuItems(Collection<MenuItemDTO> menuItems) {
		this.menuItems = menuItems;
	}
	 
}
