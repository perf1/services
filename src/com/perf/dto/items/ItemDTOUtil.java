package com.perf.dto.items;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.perf.model.menuItems.MenuItem;
import com.perf.model.products.Category;
import com.perf.model.products.ProductAttributeValue;
import com.perf.repository.menuItems.MenuItemRepository;
import com.perf.repository.products.AttributeRepository;
import com.perf.repository.products.ProductAttributeValueRepository;
@Component
public class ItemDTOUtil {
	// BeanUtils.copyProperties(retValue, target, ignoreProperties);
	@Autowired
	AttributeRepository attributeRepository;
	@Autowired
	ProductAttributeValueRepository productAttributeValueRepository;
	@Autowired
	MenuItemRepository menuItemRepository;
	
	public    CategoryDTO convertDTO(Category category ) {
		CategoryDTO catDto = new CategoryDTO();
		BeanUtils.copyProperties(category, catDto);
		
		catDto = getItemsAttribute(catDto);
		
		
		return catDto;
	}

	private   CategoryDTO getItemsAttribute(CategoryDTO catDto) {
		Collection<CategoryDTO> copiedChildCategories = new HashSet<CategoryDTO>();
		for(Object catObj: catDto.getChildCategories()) {
			CategoryDTO cDTO = new CategoryDTO();
			if(catObj instanceof CategoryDTO) {
				cDTO = (CategoryDTO)catObj;
			}else {
				BeanUtils.copyProperties(catObj, cDTO);
			}
			
			cDTO=this.getItemsAttribute(cDTO);
			copiedChildCategories.add(cDTO);
		}
		if(copiedChildCategories.size()>0) {
			catDto.setChildCategories(copiedChildCategories);
		}
		
		Collection<MenuItemDTO> items = catDto.getMenuItems();
		Collection<MenuItemDTO> copiedItems = new HashSet<MenuItemDTO>();
		for(Object itemObj :items) {
			MenuItemDTO mDto = new MenuItemDTO();
			if(itemObj instanceof MenuItemDTO) {
				mDto = (MenuItemDTO)itemObj;
			}else {
				BeanUtils.copyProperties(itemObj, mDto);
			}
			Collection<AttributeDTO> attributes = getItemAttribute(mDto.getMenuItemId());
			mDto.setAttributes(attributes);
			copiedItems.add(mDto);
		}
		catDto.setMenuItems(copiedItems);
		
		
		
		return catDto;
		
	}

	private   Collection<AttributeDTO> getItemAttribute(Long menuItemId) {
		Optional<MenuItem> menuItemOpt = menuItemRepository.findById(menuItemId);
		Set<AttributeDTO> sets = new HashSet<AttributeDTO> ();
		if(menuItemOpt.isPresent()) {
			MenuItem menuItem = menuItemOpt.get();
			Collection<ProductAttributeValue> attrValues = productAttributeValueRepository.findByMenuItem(menuItem);
			
			for(ProductAttributeValue prodAttr: attrValues) {
				AttributeDTO catDto = new AttributeDTO();
				catDto.setAttributeId(prodAttr.getAttribute().getAttributeId());
				catDto.setAttributeValueId(prodAttr.getAttributeValue().getAttributeValueId());
				catDto.setName(prodAttr.getAttribute().getName());
				catDto.setDescription(prodAttr.getAttribute().getDescription());
				catDto.setValue(prodAttr.getAttributeValue().getValue());
				 
				sets.add(catDto);
			}			
		} 		
		return sets;
	}

}
