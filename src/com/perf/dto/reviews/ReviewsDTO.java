package com.perf.dto.reviews;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class ReviewsDTO { 
	String userId;
	String menuItemId; 
	String message;
	String username; 
	String reviewId; 
	Timestamp timestamp;	
	Long displayDate; 
    private MultipartFile image;
	private String title;
    private String description;

    
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public Long getDisplayDate() {
		return displayDate;
	}
	public void setDisplayDate(Long displayDate) {
		this.displayDate = displayDate;
	}
	public String getReviewId() {
		return reviewId;
	}
	public void setReviewId(String reviewId) {
		this.reviewId = reviewId;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMenuItemId() {
		return menuItemId;
	}
	public void setMenuItemId(String menuItemId) {
		this.menuItemId = menuItemId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	} 
	
	
}
