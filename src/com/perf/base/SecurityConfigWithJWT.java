package com.perf.base;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.perf.security.rest.JwtAuthenticationFilter;
import com.perf.security.rest.JwtAuthenticationProvider;
import com.perf.security.rest.JwtTokenService;
import com.perf.security.rest.RestAuthenticationEntryPoint;
import com.perf.utils.AntMatcherUtil;

/**
 * @author Allan Xu May 12, 2019
 * 
 *         Use Json Web Token for authentication token Set stateless session,
 *         disable CSRF
 */
@Configuration
@EnableWebSecurity
@ComponentScan("com.perf.security.*")
@EnableAutoConfiguration

public class SecurityConfigWithJWT extends WebSecurityConfigurerAdapter {

	@Autowired
	JwtAuthenticationProvider jwtAuthenticationProvider;

	@Autowired
	JwtTokenService jwtTokenService;

	@Autowired
	RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	@Value("${access.admin.role}")
	private String ADMIN_ROLE;
	@Value("${access.user.role}")
	private String USER_ROLE;
	@Value("${access.permitAll}")
	private String PATHS_REQUEST_PERMIT_ALL;
	@Value("${access.authenticated.all}")
	private String PATHS_REQUEST_AUTHENTICATED_ALL;
	@Value("${access.authenticated.adminrole}")
	private String PATHS_REQUEST_AUTHENTICATED_ADMIN_ROLE;
	@Value("${access.authenticated.userrole}")
	private String PATHS_REQUEST_AUTHENTICATED_USER_ROLE;
	@Value("${access.ignoreAll}")
	private String PATHS_REQUEST_IGNORE_ALL;
	@Value("${endpoints.cors.allowed-headers}")
	private String CORS_ALLOWED_HEADERS;
	@Value("${endpoints.cors.allowed-methods}")
	private String CORS_ALLOWED_METHODS;
	@Value("${endpoints.cors.allowed-origins}")
	private String CORS_ALLOWED_ORIGINS;
	@Value("${endpoints.cors.exposed-headers}")
	private String CORS_EXPOSED_HEADERS;
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		// Use customized authentication provider
		auth.authenticationProvider(jwtAuthenticationProvider);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// disable csrf for all RESTful application, add our custom JWT filter
		http.cors();
		http.csrf().disable().formLogin().disable();
		http.addFilterBefore(getJwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

		// Set permitAll, authenticate all through properties
		http.authorizeRequests().antMatchers(PATHS_REQUEST_PERMIT_ALL.split(",")).permitAll()
				.antMatchers(PATHS_REQUEST_AUTHENTICATED_ADMIN_ROLE.split(",")).hasRole(ADMIN_ROLE)
				.antMatchers(PATHS_REQUEST_AUTHENTICATED_USER_ROLE.split(",")).hasAnyRole(USER_ROLE, ADMIN_ROLE) // hasAuthority("READ_PRIVILEGE")
				.antMatchers(PATHS_REQUEST_AUTHENTICATED_ALL.split(",")).authenticated();

		// Set session stateless for REST, and redirect entry point for not redirecting.
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint);

	} 	
			
	 @Bean
	    CorsConfigurationSource corsConfigurationSource()
	    {
	        CorsConfiguration configuration = new CorsConfiguration();

	        configuration.setAllowedOrigins(Arrays.asList(CORS_ALLOWED_ORIGINS.split(",")));
	        configuration.setAllowedMethods(Arrays.asList(CORS_ALLOWED_METHODS.split(",")));
	        configuration.setAllowedHeaders(Arrays.asList(CORS_ALLOWED_HEADERS.split(",")));
	        configuration.setExposedHeaders(Arrays.asList(CORS_EXPOSED_HEADERS.split(",")));
	        
	        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	        source.registerCorsConfiguration("/**", configuration);
	        return source;
	    }
	// Init JWT fitler
	public JwtAuthenticationFilter getJwtAuthenticationFilter() throws Exception {
		List<RequestMatcher> ignoreMatchers = AntMatcherUtil.antMatchers(null, PATHS_REQUEST_PERMIT_ALL.split(","));
		JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter();
		jwtAuthenticationFilter.setJwtTokenService(jwtTokenService);
		jwtAuthenticationFilter.setAuthenticationManager(authenticationManagerBean());
		jwtAuthenticationFilter.setIgnorePatchMatchers(ignoreMatchers);

		return jwtAuthenticationFilter;
	}

	// configure global ignore path for static paths
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(PATHS_REQUEST_IGNORE_ALL.split(","));
	}
}
