package com.perf.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
//@WebAppConfiguration
@ComponentScan(basePackages = {"com.perf"} )
@EnableConfigurationProperties
@EnableJpaRepositories( "com.perf.repository" )

@EntityScan( "com.perf.model" )
@PropertySources({
    @PropertySource("classpath:application.properties"),
    @PropertySource("classpath:datasource.properties")
})

//@ImportResource("classpath:app-config.xml")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class,  args);
	}

}
