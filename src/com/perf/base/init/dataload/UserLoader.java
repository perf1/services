package com.perf.base.init.dataload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.perf.model.security.Role;
import com.perf.model.users.User;
import com.perf.repository.security.RoleRepository;
import com.perf.repository.users.UserRepository;

@Component
public class UserLoader extends DataLoader {
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public void loadRecord(String[] record) {
		String userLogonId = record[0];
		String password = record[1];
		String roleNames = record[2];
		List<Role> roles = new ArrayList<Role>();
		for (String roleName : roleNames.split(this.COMMA_DELIMITER)) {
			Role role = roleRepository.findByName(roleName);
			if (role != null) {
				roles.add(role);
			}
		}

		Optional<User> userOpt = userRepository.findByLogonId(userLogonId);
		User user = null;
		if (userOpt.isPresent()) {
			user = userOpt.get();

		} else {
			user = new User();
			user.setLogonId(userLogonId);
			if (password == null || password.trim().isEmpty()) {
				password = "test";
			}
			user.setPassword(passwordEncoder.encode(password));
		}
		user.setRoles(roles);
		user.setEnabled(true);

		userRepository.save(user);

	}

}
