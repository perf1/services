package com.perf.base.init.dataload;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.perf.model.menuItems.MenuItem;
import com.perf.model.products.Attribute;
import com.perf.model.products.AttributeValue;
import com.perf.model.products.ProductAttributeValue;
import com.perf.repository.menuItems.MenuItemRepository;
import com.perf.repository.products.AttributeRepository;
import com.perf.repository.products.AttributeValueRepository;
import com.perf.repository.products.ProductAttributeValueRepository;

@Component
public class ItemAttributeLoader extends DataLoader {
	@Autowired
	AttributeValueRepository attributeValueRepository;
	@Autowired
	AttributeRepository attributeRepository;
	@Autowired
	MenuItemRepository menuItemRepository;
	@Autowired
	ProductAttributeValueRepository productAttributeValueRepository;
  

	@Override
	public void loadRecord(String[] record) {
		 
//		identifier,attr_name,value,sequence,delete
//		f01,calories,527g,,
		String name= getColumnValue(record,"attr_name");  
		String value = getColumnValue(record,"value");
		String delete = getColumnValue(record,"delete");
		String sequence = getColumnValue(record,"sequence");
		String identifier = getColumnValue(record,"identifier");
		
		
		Optional<MenuItem> optionalMenuItem  = menuItemRepository.findByIdentifier(identifier) ;		
		Optional<Attribute> optionalAttr  = attributeRepository.findByName(name);		
		Attribute attribute = optionalAttr.get(); 
		
		Optional<AttributeValue> optionalAttrVal  = attributeValueRepository.findByValueAndAttribute(value, attribute);
		if(!optionalMenuItem.isPresent() || !optionalAttr.isPresent() ) return;
		AttributeValue attrVal  ;
		if(! optionalAttrVal.isPresent()) {
			  attrVal = new AttributeValue();
			  attrVal.setAttribute(attribute);
			  attrVal.setValue(value);
			  attributeValueRepository.save(attrVal);
		}else {
			  attrVal = optionalAttrVal.get();
		}
		MenuItem menuItem = optionalMenuItem.get();
				
		
		Optional<ProductAttributeValue> optionalProductAttributeValue = 
				productAttributeValueRepository.findByMenuItemAndAttributeValue(menuItem, attrVal);
		
		if("y".equalsIgnoreCase(delete) &&   optionalProductAttributeValue.isPresent()) {
			productAttributeValueRepository.delete(optionalProductAttributeValue.get());
		}
		
		if(!optionalProductAttributeValue.isPresent()) {
			ProductAttributeValue pav = new ProductAttributeValue();
			pav.setAttribute(attribute);
			pav.setAttributeValue(attrVal);
			pav.setMenuItem(menuItem);
			if(sequence!=null && !sequence.isEmpty())
			pav.setSequence(Integer.valueOf(sequence));
			productAttributeValueRepository.save(pav);
		}
		 
		

	}

}
