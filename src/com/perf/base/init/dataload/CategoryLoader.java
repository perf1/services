package com.perf.base.init.dataload;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.perf.model.products.Category;
import com.perf.repository.products.CategoryRepository;

@Component
public class CategoryLoader extends DataLoader {
	@Autowired
	CategoryRepository categoryRepository;
  

	@Override
	public void loadRecord(String[] record) {
		//name,parent,isTopCategory,description
		String parent = getColumnValue(record,"parent");
		String name= getColumnValue(record,"name"); 
		String isTopCategory = getColumnValue(record,"isTopCategory");
		String description = getColumnValue(record,"description");
		String delete = getColumnValue(record,"delete");
		
		Optional<Category> optional  = categoryRepository.findByName(name);
		if("y".equalsIgnoreCase(delete) &&   optional.isPresent()) {
				categoryRepository.delete(optional.get());
		}else {
			Category item ;
			if(optional.isPresent()) {
				
				item = optional.get(); 
				 
				
			}else {
				item = new Category();
			
			}
			if(!parent.isEmpty()) {
				Optional<Category> parentCat = categoryRepository.findByName(parent);
				if(parentCat.isPresent()) {
					Category newParent = parentCat.get();
					
					Collection<Category> existingParentCats = item.getParentCategories();
					if(existingParentCats == null) {
						existingParentCats = new HashSet<Category>();
					}
					
					boolean foundMatch = false;
					for(Category c: existingParentCats) {
						if(c.getCategoryId().equals(newParent.getCategoryId())) {
							foundMatch = true;
							break;
						}
					}
					if(!foundMatch) {
						existingParentCats.add(parentCat.get());
					}
					
					item.setParentCategories(existingParentCats);
				}
			} 
			
			item.setName(name);
			item.setDescription(description);
			if(isTopCategory.equalsIgnoreCase("y")) {
				item.setTopCategory(true);
			}
			
			
			categoryRepository.save(item);
			
			
		} 
		

	}

}
