package com.perf.base.init.dataload;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.perf.model.security.Privilege;
import com.perf.model.security.Role;
import com.perf.repository.security.PrivilegeRepository;
import com.perf.repository.security.RoleRepository;
@Component
public class RoleLoader extends DataLoader{
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	PrivilegeRepository privilegeRepository;

  
		
 
	@Override
	public void loadRecord(String[] record) {
		String roleName= record[0];
		String privilegeNames= record[1];
		privilegeNames= privilegeNames.replaceAll("\"", "");
		
		
		List<Privilege> privileges = new ArrayList<Privilege>();
		for(String privilege:privilegeNames.split(this.COMMA_DELIMITER)) {
			Privilege p = privilegeRepository.findByName(privilege);
			privileges.add(p);
		}
		
		Role role = roleRepository.findByName(roleName);
		if (role == null) {
			role = new Role();
			role.setName(roleName);
			
		}
		role.setPrivileges(privileges);
		roleRepository.save(role);
	}
	
	 
	 
	
	

}
