package com.perf.base.init.dataload;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.perf.model.security.Privilege;
import com.perf.model.security.Role;
import com.perf.model.users.User;
import com.perf.repository.security.PrivilegeRepository;
import com.perf.repository.security.RoleRepository;
import com.perf.repository.users.UserRepository;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {
 

	public static final String DATA_LOAD_PATH = "dataload/";
	boolean alreadySetup = true;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PrivilegeRepository privilegeRepository;

	@Autowired
	MenuItemLoader miLoader;

	@Autowired
	PrivilegeLoader pvLoader;

	@Autowired
	RoleLoader roleLoader;
	@Autowired
	UserLoader userLoader;
	@Autowired
	CategoryLoader categoryLoader;
	
	@Autowired
	AttributeLoader attributeLoader;
	
	@Autowired
	ItemAttributeLoader itemAttributeLoader;

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {

		initPrivilegeLoad();
		initRoleLoad();

		initUserDataLoad();
		initMenuItemLoad();
		
		initCategoryLoad();
		initAttributeLoad();
		initItemAttributeValueLoad();

	}

	private void initItemAttributeValueLoad() {
		itemAttributeLoader.load(DATA_LOAD_PATH + "item-attributes.csv");
	}

	private void initAttributeLoad() {
		attributeLoader.load(DATA_LOAD_PATH + "attribute.csv");
	}

	private void initRoleLoad() {
		roleLoader.load(DATA_LOAD_PATH + "roles.csv");
	}

	private void initPrivilegeLoad() {
		pvLoader.load(DATA_LOAD_PATH + "privileges.csv");
	}

	private void initMenuItemLoad() {
		miLoader.load(DATA_LOAD_PATH + "menuitems.csv");
	}

	private void initUserDataLoad() {
		userLoader.load(DATA_LOAD_PATH + "users.csv");
	}
	

	private void initCategoryLoad() {
		categoryLoader.load(DATA_LOAD_PATH + "category.csv");
	}

}