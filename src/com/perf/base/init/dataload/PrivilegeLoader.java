package com.perf.base.init.dataload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.perf.model.security.Privilege;
import com.perf.repository.security.PrivilegeRepository;
@Component
public class PrivilegeLoader extends DataLoader{
	@Autowired
	PrivilegeRepository privilegeRepository;

 
	@Override
	public void loadRecord(String[] record) {
		String name= record[0];
		
		Privilege privilege = privilegeRepository.findByName(name);
		if (privilege == null) {
			privilege = new Privilege();
			privilege.setName(name);
			privilegeRepository.save(privilege);
		} 
	}
	
	 
	 
	
	

}
