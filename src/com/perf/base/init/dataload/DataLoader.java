package com.perf.base.init.dataload;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public abstract class DataLoader {
	protected static final String COMMA_DELIMITER = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
	protected Map<String, Integer> columnNames = new HashMap<String, Integer>();

	public void load(String fileName) {
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = br.readLine();
			resolveColumnNames(line);

			while ((line = br.readLine()) != null) {
				if(!line.trim().isEmpty()) {
					String[] values = line.split(COMMA_DELIMITER);
					loadRecord(values);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void resolveColumnNames(String firstLine) {
		String[] columns = firstLine.split(COMMA_DELIMITER);
		for (int i = 0; i < columns.length; i++) {
			if (columns[i] != null)
				columnNames.put(columns[i], i);
		}

	}

	protected String getColumnValue(String[] record, String columnName) {

		if (columnNames.containsKey(columnName)) {
			int index = this.columnNames.get(columnName);
			if (record.length > index) {
				String v = record[this.columnNames.get(columnName)];
				if(v.startsWith("\"") && v.endsWith("\"")) {
					v = v.substring(1,v.length()-1);
				}
				return v;
			}
		}

		return null;

	}

	public abstract void loadRecord(String[] record);

}
