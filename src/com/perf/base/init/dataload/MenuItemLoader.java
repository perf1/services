package com.perf.base.init.dataload;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.perf.model.menuItems.MenuItem;
import com.perf.model.products.Category;
import com.perf.repository.menuItems.MenuItemRepository;
import com.perf.repository.products.CategoryRepository;

@Component
public class MenuItemLoader extends DataLoader {
	@Autowired
	MenuItemRepository menuItemRepository;
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public void loadRecord(String[] record) {
		// identifier,category,name,description,delete
		String identifier = getColumnValue(record, "identifier");
		String name = getColumnValue(record, "name");
		String description = getColumnValue(record, "description");
		String category = getColumnValue(record, "category");
		String delete = getColumnValue(record, "delete");

		Optional<MenuItem> miO = menuItemRepository.findByIdentifier(identifier);
		Category cat = null;
		if (category != null && !category.isEmpty()) {
			Optional<Category> catOpt = categoryRepository.findByName(category);
			if (catOpt.isPresent())
				cat = catOpt.get();
		}

		if (miO.isPresent()) {

			MenuItem mi = miO.get();
			if ("y".equalsIgnoreCase(delete)) {
				menuItemRepository.delete(mi);
			} else {
				mi.setMenuItemName(name);
				mi.setMenuItemDesc(description);
				if (cat != null) {
					Set<Category> cats = new HashSet();
					cats.add(cat);
					mi.setCategories(cats);
			         
				}
					
				menuItemRepository.save(mi);
			}

		} else if (!"y".equalsIgnoreCase(delete)) {
			MenuItem mi = new MenuItem();
			mi.setIdentifier(identifier);
			mi.setMenuItemName(name);
			mi.setMenuItemDesc(description);
			if (cat != null)
				mi.setCategories(Collections.singletonList(cat));
			menuItemRepository.save(mi);
		}

	}

}
