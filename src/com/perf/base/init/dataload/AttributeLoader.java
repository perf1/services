package com.perf.base.init.dataload;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.perf.model.menuItems.MenuItem;
import com.perf.model.products.Attribute;
import com.perf.repository.products.AttributeRepository;

@Component
public class AttributeLoader extends DataLoader {
	@Autowired
	AttributeRepository attributeRepository;
  

	@Override
	public void loadRecord(String[] record) {
		//name,description,delete
		 
		String name= getColumnValue(record,"name");  
		String description = getColumnValue(record,"description");
		String delete = getColumnValue(record,"delete");
		
		Optional<Attribute> optional  = attributeRepository.findByName(name);
		if("y".equalsIgnoreCase(delete) &&   optional.isPresent()) {
				attributeRepository.delete(optional.get());
		}else {
			Attribute item ;
			if(optional.isPresent()) {
				
				item = optional.get(); 
				 
				
			}else {
				item = new Attribute();
				 
			
			}
			
			item.setName(name);
			item.setDescription(description);
			 
			
			
			attributeRepository.save(item);
			
			
		} 
		

	}

}
