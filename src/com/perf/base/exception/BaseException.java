package com.perf.base.exception;

public class BaseException extends Exception{
	
	private String className;
	private String methodName;
	private String msgKey;
	private String[] params;

	public BaseException(String className, String methodName, String msgKey) {
		super(msgKey);
		this.className= className;
		this.methodName = methodName;
		this.msgKey = msgKey;
	}
	public BaseException(String className, String methodName, String msgKey, String[] params) {
		super(msgKey);
		this.className= className;
		this.methodName = methodName;
		this.msgKey = msgKey;
		this.params = params;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMsgKey() {
		return msgKey;
	}

	public void setMsgKey(String msgKey) {
		this.msgKey = msgKey;
	}
	public String[] getParams() {
		return params;
	}
	public void setParams(String[] params) {
		this.params = params;
	}
	

}
