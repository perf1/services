package com.perf.base.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.perf.base.exception.BaseException;

/**
 * @author Allan Xu Jan 5, 2019 This util class will be used for different aop
 *         point cut service.
 *
 */
@Aspect
@Component
public class AOPUtil {
	@Before("@annotation(RequestValidation)")
	public void before(JoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();

		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		// if method is annotated with @Test
		if (method.isAnnotationPresent(RequestValidation.class)) {

			RequestValidation requestValidation = (RequestValidation) method.getAnnotation(RequestValidation.class);
			String[] requiredList = requestValidation.required();
			 
			for(String requiredParam:requiredList) {
				boolean foundParam = false;
				for(Object arg: args) {
					if(arg instanceof String || arg instanceof  Long || arg instanceof  Integer  ) {
						if(arg.toString().equals(requiredParam)) {
							foundParam = true;
							  
						}
					}else if (arg instanceof  Object[]){
						// Check single level array only
						 for(Object o : (Object[])arg) {
							 if(o.toString().equals(requiredParam)) {
									foundParam = true;
									break;
								} 
						 }
					}else if (arg instanceof  Map){
						 for(Object o : ((Map)arg).keySet()) {
							 if(o.toString().equals(requiredParam)) {
									foundParam = true;
									break;
								} 
						 }
					}
					if(foundParam) break;
				}
				if(!foundParam) {
					throw new BaseException(this.getClass().getName(),"requestValidation.before",
							"error.request.validation.param.required",new String[] {requiredParam});
				}
			
			}
		}

	}

	@Around("@annotation(DTOConverter)")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {

		// System.out.println("AOP around");

		Object retValue = joinPoint.proceed();
		Object result = retValue;
		try {

			if (retValue != null && retValue instanceof Collection) {
				result = new ArrayList<>();
				for (Object r : (Collection) retValue) {
					Object converted = dtoConvert(joinPoint, r);
					((List) result).add(converted);
				}

			} else {

				result = dtoConvert(joinPoint, retValue);
			}

		} catch (Exception e) {
		}

		if (result == null) {
			result = new ResponseEntity(HttpStatus.NOT_FOUND);

		}
		return result;
	}

	private Object dtoConvert(ProceedingJoinPoint joinPoint, Object retValue) throws Exception {

		Class obj = joinPoint.getTarget().getClass();
		String methodName = joinPoint.getSignature().getName();

		for (Method method : obj.getDeclaredMethods()) {
			if (method.getName().equals(methodName)) {
				// if method is annotated with @Test
				if (method.isAnnotationPresent(DTOConverter.class)) {

					Annotation annotation = method.getAnnotation(DTOConverter.class);
					DTOConverter dtoConverter = (DTOConverter) annotation;

					String[] ignoreProperties = dtoConverter.ignore();
					Class targetClazz = dtoConverter.to();
					if (!targetClazz.getName().equals(Object.class.getName())) {
						Object target = targetClazz.newInstance();
						BeanUtils.copyProperties(retValue, target, ignoreProperties);
						return target;
					} else {
						return convertToJSON(retValue, ignoreProperties);
					}

				}
			}
		}
		return null;
	}

	private Object convertToJSON(Object retValue, String[] ignoreProperties) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(retValue);
		if (ignoreProperties.length > 0) {
			JacksonJsonParser jsonParser = new JacksonJsonParser();
			Map map = jsonParser.parseMap(jsonString);
			for (String ignoreKey : ignoreProperties) {
				if (map.containsKey(ignoreKey)) {
					map.put(ignoreKey, "");
				}
			}
			return map;
		}
		return jsonString;
	}

}
