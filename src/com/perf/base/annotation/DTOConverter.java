package com.perf.base.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

 
@Retention(RUNTIME)
@Target(METHOD)
/**
 * Use this annotation for converting Entity into DTO.
 * @author Allan Xu
 * May 22, 2019 
 */
public @interface DTOConverter {
	 public String[] ignore() default {""}; 
	 public Class to() default Object.class;
}
