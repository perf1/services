package com.perf.base;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

/**
 * Adding a converter to allow fetch the lazy-loading attributes 
 * @author Allan Xu
 * Jun 22, 2019
 */
@Configuration
public class JsonConfig {
	
	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
	    MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
	    ObjectMapper objectMapper = jsonConverter.getObjectMapper();
	    objectMapper.registerModule(new Hibernate5Module());


	    return jsonConverter;
	}

}
