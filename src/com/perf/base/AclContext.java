package com.perf.base;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;

import com.perf.model.security.acl.CustomPermissionEvaluator;

@Configuration
@EnableAutoConfiguration
public class AclContext {
	@Bean
	public MethodSecurityExpressionHandler defaultMethodSecurityExpressionHandler() {
		DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
		// customize this evaluator
		CustomPermissionEvaluator permissionEvaluator = new CustomPermissionEvaluator();
		expressionHandler.setPermissionEvaluator(permissionEvaluator);
//		expressionHandler.setPermissionCacheOptimizer(new AclPermissionCacheOptimizer(aclService()));
		return expressionHandler; 
	}
	// @Autowired
	// DataSource dataSource;
	//
	// @Bean
	// public EhCacheBasedAclCache aclCache() {
	// return new EhCacheBasedAclCache(aclEhCacheFactoryBean().getObject(),
	// permissionGrantingStrategy(),
	// aclAuthorizationStrategy());
	// }
	//
	// @Bean
	// public EhCacheFactoryBean aclEhCacheFactoryBean() {
	// EhCacheFactoryBean ehCacheFactoryBean = new EhCacheFactoryBean();
	// ehCacheFactoryBean.setCacheManager(aclCacheManager().getObject());
	// ehCacheFactoryBean.setCacheName("aclCache");
	// return ehCacheFactoryBean;
	// }
	//
	// @Bean
	// public EhCacheManagerFactoryBean aclCacheManager() {
	// return new EhCacheManagerFactoryBean();
	// }
	//
	// @Bean
	// public PermissionGrantingStrategy permissionGrantingStrategy() {
	// return new DefaultPermissionGrantingStrategy(new ConsoleAuditLogger());
	// }

	// @Bean
	// public AclAuthorizationStrategy aclAuthorizationStrategy() {
	// return new AclAuthorizationStrategyImpl(new
	// SimpleGrantedAuthority("ROLE_ADMIN"));
	// }

	// @Bean
	// public LookupStrategy lookupStrategy() {
	// return new BasicLookupStrategy(dataSource, aclCache(),
	// aclAuthorizationStrategy(), new ConsoleAuditLogger());
	// }
	//
	// @Bean
	// public JdbcMutableAclService aclService() {
	// return new JdbcMutableAclService(dataSource, lookupStrategy(), aclCache());
	// }
}
