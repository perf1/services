package com.perf.base;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.perf.base.exception.BaseException;
import com.perf.constant.Constants;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(value = { BaseException.class, Exception.class })
	protected ResponseEntity<Object> handleConflict( Exception ex, WebRequest request) {
		Map <String, String> resp = new HashMap<String, String>();
		if(ex instanceof BaseException) {
			BaseException bex = (BaseException)ex;
			ResourceBundle msgBundle = ResourceBundle.getBundle("messages");
			String msg = msgBundle.getString(bex.getMsgKey()) ;
			if(bex.getParams()!=null) {
				MessageFormat form = new MessageFormat(msg);
				msg = form.format(bex.getParams());
				
			}
			resp.put(Constants.ERROR, msg);
		}
		 
		return handleExceptionInternal(ex, resp, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
}
