package com.perf.repository.order;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.perf.model.order.OrderItem;
@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Long>{

	@Query(value = "SELECT * FROM Order_Item orderitem " +
          "WHERE orderitem.orders_id = :orders_id", nativeQuery = true)
	List<OrderItem> findByOrdersId(Long orders_id);
}
