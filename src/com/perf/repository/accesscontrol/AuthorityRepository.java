package com.perf.repository.accesscontrol;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.users.User;
@Repository
public interface AuthorityRepository extends JpaRepository<User, Long> {
 
	User findByLogonId(String logonId);
}