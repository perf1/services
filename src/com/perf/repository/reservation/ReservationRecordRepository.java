package com.perf.repository.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.reservation.ReservationRecord;

@Repository
public interface ReservationRecordRepository extends JpaRepository<ReservationRecord, Long>{
	
}
