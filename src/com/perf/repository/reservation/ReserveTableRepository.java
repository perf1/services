package com.perf.repository.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.reservation.ReserveTable;

@Repository
public interface ReserveTableRepository extends JpaRepository<ReserveTable, Long>{

}
