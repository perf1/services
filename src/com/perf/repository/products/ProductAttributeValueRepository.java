package com.perf.repository.products;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.menuItems.MenuItem;
import com.perf.model.products.AttributeValue;
import com.perf.model.products.ProductAttributeValue;

@Repository
public interface ProductAttributeValueRepository extends JpaRepository<ProductAttributeValue, Long> {
	Optional<ProductAttributeValue> findByMenuItemAndAttributeValue(MenuItem menuItem, AttributeValue attributeValue); 
	Collection<ProductAttributeValue> findByMenuItem(MenuItem menuItem); 
 
}
