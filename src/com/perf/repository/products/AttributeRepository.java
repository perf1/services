package com.perf.repository.products;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.products.Attribute;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long> {

	Optional<Attribute> findByName(String name);
 
}
