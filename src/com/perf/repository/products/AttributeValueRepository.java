package com.perf.repository.products;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.products.Attribute;
import com.perf.model.products.AttributeValue;

@Repository
public interface AttributeValueRepository extends JpaRepository<AttributeValue, Long> {
	Optional<AttributeValue> findByValueAndAttribute(String value, Attribute attribute);
 
}
