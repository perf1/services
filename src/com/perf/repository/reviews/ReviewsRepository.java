package com.perf.repository.reviews;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.reviews.Reviews;

@Repository
public interface ReviewsRepository extends JpaRepository<Reviews, Long> {
}