package com.perf.repository.menuItems;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.menuItems.MenuItem;
import com.perf.model.products.Category;

@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {

	Optional<MenuItem> findByMenuItemName(String name);
	Optional<MenuItem> findByIdentifier(String identifier);
	 

}
