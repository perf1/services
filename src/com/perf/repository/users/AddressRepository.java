package com.perf.repository.users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.perf.model.users.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
	@Query(value="SELECT * FROM Address a WHERE a.user_id = :userId and a.nick_name=:nickName and a.status='P' " ,nativeQuery = true)
	Address findByUserIdAndNickName(@Param("userId") Long userId, @Param("nickName") String nickName);

	@Query(value="SELECT * FROM Address a WHERE a.user_id = :userId and a.is_profile = 'Y' and a.status='P' ",nativeQuery = true)
	public Address findProfileAddressByUserId(@Param("userId") Long userId);
	
	@Query(value="SELECT * FROM Address a WHERE a.user_id = :userId  and a.status='P' ",nativeQuery = true)
	List<Address> findAllByUserId(@Param("userId") Long userId);

}