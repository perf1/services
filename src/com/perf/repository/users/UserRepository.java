package com.perf.repository.users;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perf.model.users.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByLogonId(String logonId);
}