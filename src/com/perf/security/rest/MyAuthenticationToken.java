package com.perf.security.rest;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * The custom authentication token 
 * @author Allan Xu
 * May 14, 2019 
 */
public class MyAuthenticationToken extends UsernamePasswordAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	

	private final String  logonId;
	private Long userId;

	public MyAuthenticationToken( String  logonId, Long userId, Collection<? extends GrantedAuthority> authorities) {
		super(logonId, null, authorities);
		this.logonId = logonId;
		this.userId = userId;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public String getPrincipal() {
		return logonId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	

}