package com.perf.security.rest;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.perf.model.users.User;

public class MyUserPrincipal implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String password;
	private boolean accountNonExpired = true;
	private boolean accountNonLocked =true;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;
	private User user; 
	private Long userId;
	
	Collection<? extends GrantedAuthority> authorities;
	public MyUserPrincipal(User user, Collection<? extends GrantedAuthority> authorities) {
		this.name = user.getLogonId();
		this.password = user.getPassword();
		this.authorities = authorities;
		enabled = user.isEnabled();
		accountNonExpired = user.isAccountNonExpired();
		accountNonLocked = user.isAccountNonLocked();
		credentialsNonExpired = user.isCredentialsNonExpired();
		this.user = user; 
	}
	
	public MyUserPrincipal(String logonId, Collection<? extends GrantedAuthority> authorities) {
		this.name = logonId;
		this.password = null;
		this.authorities = authorities;  
	}

	public MyUserPrincipal(String logonId) {
		this.name = logonId;	 
	}

	 

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		return authorities;
	}

	@Override
	public String getPassword() {

		return password;
	}

	@Override
	public String getUsername() {

		return name;
	}

	@Override
	public boolean isAccountNonExpired() {

		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {

		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {

		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {

		return enabled;
	}

 
	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

 

}
