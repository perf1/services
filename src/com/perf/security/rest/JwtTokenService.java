package com.perf.security.rest;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import com.perf.model.users.User;

public interface JwtTokenService {

	Authentication createAuthenticationFromJwtToken(String jwtToken) throws AuthenticationException;

//	String createJwtTokenFromAuthentication(Authentication authentication);

//	String createJwtToken(String userName, Collection<? extends GrantedAuthority> authorities);

	String createJwtToken(User user, Collection<? extends GrantedAuthority> authorities);

}
