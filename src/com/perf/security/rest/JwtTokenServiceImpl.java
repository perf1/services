package com.perf.security.rest;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import com.perf.constant.SecurityConstants;
import com.perf.model.users.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author Allan Xu
 * 
 *         May 14, 2019
 * 
 *         Provide the service to convert from JWT token to Spring
 *         authentication token, or vice versa. The JWT token can carry
 *         authorization data, so the service provider does not need to go in
 *         database or external system to verify user roles and permissions for
 *         each request.
 */
@Service
public class JwtTokenServiceImpl implements JwtTokenService {

	@Value("${jwt.secret}")
	private String secret;
	@Value("${jwt.access.token.expiration.minutes}")
	private long EXPIRATION_TIME_IN_MINUTES;

	

	/**
	 * Parse the JWT token, and convert it into a Spring Authentication.
	 * 
	 * @param token
	 *            the JWT token to parse
	 * @return the User object extracted from specified token or null if a token is
	 *         invalid.
	 */
	@Override
	public Authentication createAuthenticationFromJwtToken(String token) throws AuthenticationException {

		try {
			Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();

			// Generate GrantedAuthory from the claims
			Collection<GrantedAuthority> authorities = Arrays.stream(String.valueOf(claims.get(SecurityConstants.AUTHORITIES)).split(","))
					.map(String::trim).map(String::toUpperCase).map(SimpleGrantedAuthority::new)
					.collect(Collectors.toSet());

			// Get userId from claims
			String logonId = claims.getSubject();
			String userId = claims.get(SecurityConstants.USER_ID).toString();

			// Create Spring authentication Token
			MyAuthenticationToken jwtAuthenticationToken = new MyAuthenticationToken(logonId, Long.valueOf(userId), authorities);

			Date now = new Date();
			Date expiration = claims.getExpiration();
			Date notBefore = claims.getNotBefore();
			boolean valid = notBefore != null ? now.after(notBefore)
					: true && expiration != null ? now.before(expiration) : true;
			if (!valid)
				jwtAuthenticationToken.setAuthenticated(valid);

			return jwtAuthenticationToken;

		} catch (JwtException | ClassCastException e) {
			throw new MyAuthenticationException("Authentication failed.");
		}
	}

	@Override
	public String createJwtToken(User user, Collection<? extends GrantedAuthority> authorities) {
		Claims claims = Jwts.claims().setSubject(user.getLogonId())
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME_IN_MINUTES * 60 * 1000))
				.setIssuedAt(new Date());

		// The authorities is from authentication
		if (authorities != null) {
			String authoritiesStr = authorities.stream().map(GrantedAuthority::getAuthority).map(String::toUpperCase)
					.collect(Collectors.joining(","));
			claims.put(SecurityConstants.AUTHORITIES, authoritiesStr);
			claims.put(SecurityConstants.USER_ID, user.getUserId());
		}
		
//		claims.put(SecurityConstants.USER, user.getUserId()+":"+user.is);
//		
		
		
		
		String token = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secret).compact();
		return token;

	}

//	@Override
//	public String createJwtTokenFromAuthentication(Authentication authentication) {
//
//		return createJwtToken(authentication.getName(), authentication.getAuthorities());
//	}

}