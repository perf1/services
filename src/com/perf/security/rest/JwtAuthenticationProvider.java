package com.perf.security.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	@Autowired
	JwtTokenService jwtTokenService;

	@Override
	public boolean supports(Class<?> authentication) {
		return (MyAuthenticationToken.class.isAssignableFrom(authentication));
	}

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {

		MyAuthenticationToken myAuthenticationToken = (MyAuthenticationToken) authentication;
		MyUserPrincipal myUserPrincipal = new MyUserPrincipal(myAuthenticationToken.getPrincipal().toString(),
				 authentication.getAuthorities());
		myUserPrincipal.setUserId(myAuthenticationToken.getUserId());
		return myUserPrincipal;
	}

}
