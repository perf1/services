package com.perf.security.rest;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import com.perf.constant.SecurityConstants;

public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	JwtTokenService jwtTokenService;

	private List<RequestMatcher> ignorePatternMatchers;

	public JwtAuthenticationFilter() {
//		check every request by default. To skip this, set through ignorePatternMatchers
		super( "/**");
	}

	@Override
	protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
		if (ignorePatternMatchers != null) {
			boolean match = false;
			for (RequestMatcher matcher : ignorePatternMatchers) {
				match = matcher.matches(request);
				// found match, skip authentication
				if (match)
					return false;
			}
			 
		}
		
		// If not ignore this path, let default handler processes it
		return super.requiresAuthentication(request, response);
	 

	}

	/*
	 * Through attempt authentication process, get the request JWT Token, convert
	 * into Spring authentication token, and let Spring to authenticate it.
	 * 
	 * Externally, it is carrying JWT token. Internally, it is authenticating
	 * through Spring framework.
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		String header = request.getHeader(SecurityConstants.AUTHORIZATION);

		if (header == null || !header.startsWith(SecurityConstants.JWT_BEARER_TOKEN_PREFIX)) {
			throw new MyAuthenticationException("No JWT token found in request headers");
			// return null;?
		}

		String jwtToken = header.substring(SecurityConstants.BEARER_PREFIX_LENGTH);
		Authentication authentication = jwtTokenService.createAuthenticationFromJwtToken(jwtToken);

		return getAuthenticationManager().authenticate(authentication);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
//		super.successfulAuthentication(request, response, chain, authResult);
		SecurityContextHolder.getContext().setAuthentication(authResult);
		// As this authentication is in HTTP header, after success we need to continue
		// the request normally
		// and return the response as if the resource was not secured at all
		chain.doFilter(request, response);
	}

	public void setIgnorePatchMatchers(List<RequestMatcher> matchers) {
		this.ignorePatternMatchers = matchers;
	}

	public JwtTokenService getJwtTokenService() {
		return jwtTokenService;
	}

	public void setJwtTokenService(JwtTokenService jwtTokenService) {
		this.jwtTokenService = jwtTokenService;
	}
}