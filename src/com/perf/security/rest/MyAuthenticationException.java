package com.perf.security.rest;

import org.springframework.security.core.AuthenticationException;

public class MyAuthenticationException extends AuthenticationException {


	private static final long serialVersionUID = 3003679796479388623L;
	public MyAuthenticationException(String msg) {
		super(msg);
		 
	}


}
