//package com.perf.security.base;
//
//import java.util.Collection;
//
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//import com.perf.model.users.User;
//
//public class MyUserPrincipal implements UserDetails {
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	private String name;
//	private String password;
//	private boolean accountNonExpired = false;
//	private boolean accountNonLocked =false;
//	private boolean credentialsNonExpired = false;
//	private boolean enabled = false;
//	private User user;
//	
//	Collection<? extends GrantedAuthority> authorities;
//	public MyUserPrincipal(User user, Collection<? extends GrantedAuthority> authorities) {
//		this.name = user.getLogonId();
//		this.password = user.getPassword();
//		this.authorities = authorities;
//		enabled = user.isEnabled();
//		accountNonExpired = user.isAccountNonExpired();
//		accountNonLocked = user.isAccountNonLocked();
//		credentialsNonExpired = user.isCredentialsNonExpired();
//		this.user = user;
//	}
//
//	public MyUserPrincipal(String logonId, String password, Collection<? extends GrantedAuthority> authorities) {
//		this.name = logonId;
//		this.password = password;
//		this.authorities = authorities;
//		 
//	}
//
//	 
//
//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//
//		return authorities;
//	}
//
//	@Override
//	public String getPassword() {
//
//		return password;
//	}
//
//	@Override
//	public String getUsername() {
//
//		return name;
//	}
//
//	@Override
//	public boolean isAccountNonExpired() {
//
//		return accountNonExpired;
//	}
//
//	@Override
//	public boolean isAccountNonLocked() {
//
//		return accountNonLocked;
//	}
//
//	@Override
//	public boolean isCredentialsNonExpired() {
//
//		return credentialsNonExpired;
//	}
//
//	@Override
//	public boolean isEnabled() {
//
//		return enabled;
//	}
//
// 
//	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
//		this.authorities = authorities;
//	}
//
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//}
