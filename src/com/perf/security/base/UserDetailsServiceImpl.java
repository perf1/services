//package com.perf.security.base;
//
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import com.perf.model.users.User;
//import com.perf.repository.users.UserRepository;
//
//@Service
//public class UserDetailsServiceImpl implements UserDetailsService {
//	
//	@Autowired 
//	UserRepository userRepository;
//	
//	@Override
//	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
//		Optional<User> user = userRepository.findByLogonId(userName);
//		if(user.isPresent()) {
//			return new MyUserPrincipal(user.get());
//		}
//		throw new UsernameNotFoundException(userName);
//	}
//	
//
//
//}
