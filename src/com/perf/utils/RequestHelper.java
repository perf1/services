package com.perf.utils;

import java.math.BigDecimal;
import java.util.Map;

public class RequestHelper {
	Map map;
	public RequestHelper(Map map) {
		this.map = map;
	}
	
	private  Object get(String key, Object defaultValue) {
		if(map.containsKey(key)) {
			return map.get(key);
		}else {
			return defaultValue;
		}
	}

	
	public  Long getLong(String key ) {
		return  this.getLong(key, null);		
	}
	
	public  Long getLong(String key, Long defaultValue) {
		Object value = this.get(key, defaultValue);
		return (Long)value; 
	}
	
	public  String getString(String key ) {
		return  this.getString(key, null);		
	}
	
	public  String getString(String key, Long defaultValue) {
		Object value = this.get(key, defaultValue);
		return (String)value; 
	}
	
	
	public  Double getDouble(String key ) {
		return  this.getDouble(key, null);		
	}
	
	public  Double getDouble(String key, Double defaultValue) {
		Object value = this.get(key, defaultValue);
		return (Double)value; 
	}
	
	
	public  BigDecimal getBigDecimal(String key ) {
		return  this.getBigDecimal(key, null);		
	}
	
	
	public  BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
		Object value = this.get(key, defaultValue);
		return (BigDecimal)value; 
	}
	
	public boolean getBoolean(String key, Boolean defaultValue) {
		Object value = this.get(key, defaultValue);
		return Boolean.valueOf(value.toString());
	}
	
	public boolean getBoolean(String string) {
		
		return this.getBoolean(string, Boolean.FALSE);
	}
	
}
