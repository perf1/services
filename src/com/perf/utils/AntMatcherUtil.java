package com.perf.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class AntMatcherUtil {
	
	
	public static List<RequestMatcher> antMatchers(HttpMethod httpMethod,
			String... antPatterns) {
		String method = httpMethod == null ? null : httpMethod.toString();
		List<RequestMatcher> matchers = new ArrayList<>();
		for (String pattern : antPatterns) {
			matchers.add(new AntPathRequestMatcher(pattern, method));
		}
		return matchers;
	}


}
