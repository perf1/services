package com.perf.acl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
@Repository
public interface NoticeMessageRepository extends JpaRepository<NoticeMessage, Long>{
    
//    @PostFilter("hasPermission(filterObject, 'READ')")
    List<NoticeMessage> findAll();
    
//    @PostAuthorize("hasPermission(returnObject, 'READ')")
    Optional<NoticeMessage> findById(Long id);
    
    @SuppressWarnings("unchecked")
//    @PreAuthorize("hasPermission(#noticeMessage, 'WRITE')")
    NoticeMessage save(@Param("noticeMessage")NoticeMessage noticeMessage);

}