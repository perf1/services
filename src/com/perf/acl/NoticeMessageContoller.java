package com.perf.acl;

import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/msg")
@Transactional
public class NoticeMessageContoller {

	@Autowired 
	NoticeMessageRepository  noticeMessageRepository;
	@RequestMapping("/add")
	public NoticeMessage addNoticeMessage(@RequestParam Map<String,String> requestMap ) {
		
		String content = requestMap.get("content");
		String id = requestMap.get("id");
		NoticeMessage msg = new NoticeMessage();
		msg.setContent(content);
		msg.setId(Long.valueOf(id));
		noticeMessageRepository.save(msg);
		return msg;
	}
	
	@RequestMapping("/find/{id}")
//	@PreAuthorize("hasPermission(#id, 'READ')")
	public NoticeMessage findNoticeMessage(@PathVariable Long id ) {
		 
	 
		Optional<NoticeMessage> m = noticeMessageRepository.findById(id);
		if(m.isPresent()) {
			return m.get();
		}
		return null;
	}
}
