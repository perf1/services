package com.perf.model.users;

import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.perf.model.BaseEntity;
import com.perf.model.security.Role;


@Entity
@Table(name = "user")
@JsonIgnoreProperties({"password"})
public class User implements BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private Long userId;
	@Column(unique=true,nullable=false)
	private String logonId; 
	private String password;
	private Timestamp creationTime = new Timestamp(System.currentTimeMillis());
	private Timestamp lastUpdate = new Timestamp(System.currentTimeMillis());
	@Type(type="yes_no")
	private boolean enabled = true;	 
	@Type(type="yes_no")
	private boolean accountNonExpired = true;
	@Type(type="yes_no")
	private boolean accountNonLocked =true;
	@Type(type="yes_no")
	private boolean credentialsNonExpired = true;
	
	@ManyToMany( fetch = FetchType.EAGER)
    @JoinTable( name = "userrole", 
    			joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"), 
    			inverseJoinColumns = @JoinColumn( name = "role_id", referencedColumnName = "role_id")) 
    private Collection<Role> roles;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLogonId() {
		return logonId;
	}

	public void setLogonId(String logonId) {
		this.logonId = logonId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	 

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
	
	
 
}
