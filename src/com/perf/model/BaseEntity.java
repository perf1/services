package com.perf.model;

import java.io.Serializable;

import javax.persistence.Version;

public interface BaseEntity extends Serializable{
	@Version
	Long version = 0L;
}