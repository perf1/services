package com.perf.model.reviews;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.perf.model.menuItems.MenuItem;
import com.perf.model.users.User;

@Entity
@JsonIgnoreProperties({"user", "menuItem"})
@Table(name = "REVIEWS")
public class Reviews {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long reviewId; 
	@ManyToOne
	@JoinColumn(name="menuItemId")
	private MenuItem menuItem; 
	@ManyToOne
	private User user;
	@Column(nullable=false)
	private String message; 
	private Timestamp timestamp;
	public Long getReviewId() {
		return reviewId;
	}
	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}
	public MenuItem getMenuItem() {
		return menuItem;
	}
	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp ts) {
		this.timestamp = ts;
	} 
	
	
}
