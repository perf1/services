package com.perf.model.security.acl;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.DenyAllPermissionEvaluator;
import org.springframework.security.acls.domain.DefaultPermissionFactory;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.domain.PermissionFactory;
import org.springframework.security.acls.domain.SidRetrievalStrategyImpl;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.ObjectIdentityGenerator;
import org.springframework.security.acls.model.ObjectIdentityRetrievalStrategy;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.acls.model.SidRetrievalStrategy;
import org.springframework.security.core.Authentication;

import com.perf.security.rest.MyUserPrincipal;


/**
 * Main logic to evaluate the access control policy.
 * 
 * @author Allan Xu
 * Jun 22, 2019
 */
public class CustomPermissionEvaluator implements PermissionEvaluator {
	private final Log logger = LogFactory.getLog(getClass());
 
	private ObjectIdentityRetrievalStrategy objectIdentityRetrievalStrategy = new CustomObjectIdentityRetrievalStrategyImpl();
	private ObjectIdentityGenerator objectIdentityGenerator = new CustomObjectIdentityRetrievalStrategyImpl();
	private SidRetrievalStrategy sidRetrievalStrategy = new SidRetrievalStrategyImpl();
	private PermissionFactory permissionFactory = new DefaultPermissionFactory(CustomBasePermission.class);
	

    

	private static final PermissionEvaluator denyAll = new DenyAllPermissionEvaluator();
  
    public boolean hasPermission(Authentication authentication, Object domainObject,
			Object permission) {
		if (domainObject == null) {
			return false;
		}
		
		

		ObjectIdentity objectIdentity = objectIdentityRetrievalStrategy
				.getObjectIdentity(domainObject);

		return checkPermission(authentication, objectIdentity, permission);
	}

	public boolean hasPermission(Authentication authentication, Serializable targetId,
			String targetType, Object permission) {
		ObjectIdentity objectIdentity = objectIdentityGenerator.createObjectIdentity(
				targetId, targetType);

		return checkPermission(authentication, objectIdentity, permission);
	}
	
	private boolean checkPermission(Authentication authentication, ObjectIdentity oid,
			Object permission) {
		// Obtain the SIDs applicable to the principal
		List<Sid> sids = sidRetrievalStrategy.getSids(authentication);
		List<Permission> requiredPermission = resolvePermission(permission);

		final boolean debug = logger.isDebugEnabled();

		if (debug) {
			logger.debug("Checking permission '" + permission + "' for object '" + oid
					+ "'");
		}

		try {
			 
			//Check permission
			boolean grantedAuthorityFound = false;
			for(Object sid: sids) {
				if(sid instanceof GrantedAuthoritySid) {
					String grantedAuthority = ((GrantedAuthoritySid)sid).getGrantedAuthority();
					for(Permission p: requiredPermission) {
						String authority = grantedAuthority.toString();
						if(authority.startsWith("ROLE_")) break;
						Permission grantedPermission =  resolvePermission(authority).get(0);
						
						
						if(grantedPermission.getMask() ==  p.getMask()) {
							grantedAuthorityFound = true;
							break;
						}
					}
					if(grantedAuthorityFound) 
						break;
				}
			}
			 
			
			//Check identifier
			if(grantedAuthorityFound && oid.getIdentifier() !=null) {
				Object p = authentication.getPrincipal();
				if(p instanceof MyUserPrincipal) {
					MyUserPrincipal principal = (MyUserPrincipal)p;
					Long userId  = principal.getUserId();
					if(userId.equals(oid.getIdentifier())) {
						return true;
					}
				}
			}

			if (debug) {
				logger.debug("Returning false - ACLs returned, but insufficient permissions for this principal");
			}

		}
		catch (NotFoundException nfe) {
			if (debug) {
				logger.debug("Returning false - no ACLs apply for this principal");
			}
		}

		return false;

	}

	List<Permission> resolvePermission(Object permission) {
		if (permission instanceof Integer) {
			return Arrays.asList(permissionFactory.buildFromMask(((Integer) permission)
					.intValue()));
		}

		if (permission instanceof Permission) {
			return Arrays.asList((Permission) permission);
		}

		if (permission instanceof Permission[]) {
			return Arrays.asList((Permission[]) permission);
		}

		if (permission instanceof String) {
			String permString = (String) permission;
			Permission p; 
			
			try {
				p = permissionFactory.buildFromName(permString);
			}
			catch (IllegalArgumentException notfound) {
				p = permissionFactory.buildFromName(permString.toUpperCase(Locale.ENGLISH));
			}

			if (p != null) {
				return Arrays.asList(p);
			}

		}
		throw new IllegalArgumentException("Unsupported permission: " + permission);
	}
	

	public void setObjectIdentityRetrievalStrategy(
			ObjectIdentityRetrievalStrategy objectIdentityRetrievalStrategy) {
		this.objectIdentityRetrievalStrategy = objectIdentityRetrievalStrategy;
	}

	public void setObjectIdentityGenerator(ObjectIdentityGenerator objectIdentityGenerator) {
		this.objectIdentityGenerator = objectIdentityGenerator;
	}

	public void setSidRetrievalStrategy(SidRetrievalStrategy sidRetrievalStrategy) {
		this.sidRetrievalStrategy = sidRetrievalStrategy;
	}

	public void setPermissionFactory(PermissionFactory permissionFactory) {
		this.permissionFactory = permissionFactory;
	}
	
	
//	public Object getOwner
}
