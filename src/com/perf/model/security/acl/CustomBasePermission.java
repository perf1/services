package com.perf.model.security.acl;

import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;

public class CustomBasePermission extends BasePermission {
	/**
	 * Add more permissions
	 */
	private static final long serialVersionUID = 1L;

	public static final Permission READ_ALL = new CustomBasePermission(1 << 5, '5'); // 1
	public static final Permission WRITE_ALL = new CustomBasePermission(1 << 6, '6'); // 2
	public static final Permission CREATE_ALL = new CustomBasePermission(1 << 7, '7'); // 4
	public static final Permission DELETE_ALL = new CustomBasePermission(1 << 8, '8'); // 8

	protected CustomBasePermission(int mask) {
		super(mask);
	}

	protected CustomBasePermission(int mask, char code) {
		super(mask, code);
	}
}
