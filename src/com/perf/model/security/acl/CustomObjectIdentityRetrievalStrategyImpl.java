package com.perf.model.security.acl;

import java.io.Serializable;

import org.springframework.security.acls.domain.ObjectIdentityRetrievalStrategyImpl;
import org.springframework.security.acls.model.ObjectIdentity;

/**
 * Use customized ObjectIdentityImpl
 * @author Allan Xu
 * Jun 22, 2019
 */
public class CustomObjectIdentityRetrievalStrategyImpl extends ObjectIdentityRetrievalStrategyImpl {
	public ObjectIdentity getObjectIdentity(Object domainObject) {
		return new CustomObjectIdentityImpl(domainObject);
	}

	public ObjectIdentity createObjectIdentity(Serializable id, String type) {
		return new CustomObjectIdentityImpl(type, id);
	}
}
