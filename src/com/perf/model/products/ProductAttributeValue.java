package com.perf.model.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.perf.model.menuItems.MenuItem;

@Entity
@Table(name="product_attribute_val")
public class ProductAttributeValue {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="prod_attr_val_id")
	Long productAttributeValueId;
	
	@OneToOne
	@JoinColumn(name="attribute_val_id")
	AttributeValue attributeValue;
	
	@OneToOne
	@JoinColumn(name="item_id")
	MenuItem menuItem;
	
	
	
	@ManyToOne
	@JoinColumn(name="attribute_id")
	Attribute attribute;
	@Column(columnDefinition="integer default 0")
 	int sequence = 0;

	public Long getProductAttributeValueId() {
		return productAttributeValueId;
	}

	public void setProductAttributeValueId(Long productAttributeValueId) {
		this.productAttributeValueId = productAttributeValueId;
	}
 

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public AttributeValue getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(AttributeValue attributeValue) {
		this.attributeValue = attributeValue;
	}
	

 

  

}
