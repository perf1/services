package com.perf.model.products;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.perf.model.menuItems.MenuItem;

@Entity
@Table(name="category")
public class Category {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long categoryId;
	
	String name;
	String description;
	@Type(type="yes_no")
	boolean isTopCategory =false;
	
	@ManyToMany
	@JoinTable(name="categroy_rel",
			joinColumns = @JoinColumn(name = "category_id_child", referencedColumnName = "categoryId"), 
			inverseJoinColumns = @JoinColumn(name = "category_id_parent", referencedColumnName = "categoryId")
	)
	Collection<Category> parentCategories;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="categroy_rel",
			joinColumns = @JoinColumn(name = "category_id_parent", referencedColumnName = "categoryId"), 
			inverseJoinColumns = @JoinColumn(name = "category_id_child", referencedColumnName = "categoryId")
	)
	Collection<Category> childCategories;
	
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="item_category_rel",
	inverseJoinColumns=@JoinColumn(name="item_id", unique=false),
	joinColumns=@JoinColumn(name="category_id", unique=false))	 
	Collection<MenuItem> menuItems;
	 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public boolean isTopCategory() {
		return isTopCategory;
	}
	public void setTopCategory(boolean isTopCategory) {
		this.isTopCategory = isTopCategory;
	}
	 
	public Collection<MenuItem> getMenuItems() {
		return menuItems;
	}
	public void setMenuItems(Collection<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}
	public Collection<Category> getChildCategories() {
		return childCategories;
	}
	public void setChildCategories(Collection<Category> childCategories) {
		this.childCategories = childCategories;
	}
	public Collection<Category> getParentCategories() {
		return parentCategories;
	}
	public void setParentCategories(Collection<Category> parentCategories) {
		this.parentCategories = parentCategories;
	}

}
