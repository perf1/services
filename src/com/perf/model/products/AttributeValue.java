package com.perf.model.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="attribute_val")
public class AttributeValue {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="attribute_val_id")
	Long attributeValueId;
	String value;
	 
	
	@ManyToOne
	@JoinColumn(name="attribute_id")
	Attribute attribute;



	public Long getAttributeValueId() {
		return attributeValueId;
	}



	public void setAttributeValueId(Long attributeValId) {
		this.attributeValueId = attributeValId;
	}



	public String getValue() {
		return value;
	}



	public void setValue(String value) {
		this.value = value;
	}


 

	public Attribute getAttribute() {
		return attribute;
	}



	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}
 

}
