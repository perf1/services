package com.perf.model.menuItems;

import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.perf.model.BaseEntity;
import com.perf.model.products.Category;
import com.perf.model.reviews.Reviews;

@Entity
@Table(name = "menuitem")
public class MenuItem implements BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "menu_item_id")
	private Long menuItemId;
	@Column(unique = true, nullable = false)
	private String identifier;
	private String menuItemName;
	@Column(length=3000)
	private String menuItemDesc;
	private Timestamp creationTime = new Timestamp(System.currentTimeMillis());
	private Timestamp lastUpdate = new Timestamp(System.currentTimeMillis());
	
	@ManyToMany
	//(fetch = FetchType.EAGER)
	@JoinTable(name="item_category_rel",
	joinColumns=@JoinColumn(name="item_id", unique=false),
	inverseJoinColumns=@JoinColumn(name="category_id", unique=false))	 
	Collection<Category> categories; 

	@OneToMany(mappedBy="menuItem")
	private Collection<Reviews> reviews; 

	public Collection<Reviews> getReviews() {
		return reviews;
	}

	public void setReviews(Collection<Reviews> reviews) {
		this.reviews = reviews;
	}

	public Long getMenuItemId() {
		return menuItemId;
	}

	public void setMenuItemId(Long menuItemId) {
		this.menuItemId = menuItemId;
	}

	public String getMenuItemName() {
		return menuItemName;
	}

	public void setMenuItemName(String menuItemName) {
		this.menuItemName = menuItemName;
	}

	public Timestamp getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getMenuItemDesc() {
		return menuItemDesc;
	}

	public void setMenuItemDesc(String menuItemDesc) {
		this.menuItemDesc = menuItemDesc;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Collection<Category> getCategories() {
		return categories;
	}

	public void setCategories(Collection<Category> categories) {
		this.categories = categories;
	}

//	public Collection<AttributeValue> getProductAttributeValues() {
//		return productAttributeValues;
//	}
//
//	public void setProductAttributeValues(Collection<AttributeValue> productAttributeValues) {
//		this.productAttributeValues = productAttributeValues;
//	}

}