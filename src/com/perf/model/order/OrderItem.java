package com.perf.model.order;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.perf.model.BaseEntity;
import com.perf.model.users.User;
@Entity
@Table(name = "orderItem")
@JsonIgnoreProperties({"orders", "user"})
public class OrderItem implements BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long orderitemId;
	
	@ManyToOne
	@JoinColumn(name="orders_id")
	private Orders orders;
	
//	@OneToOne(targetEntity=MenuItem.class) 
//	@JoinColumn(name="menu_item_id")
//	private Long menuItemId; 
//	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	private Timestamp timePlaced = new Timestamp(System.currentTimeMillis());
	private Timestamp lastUpdate;// = new Timestamp(System.currentTimeMillis());
	private Timestamp estAvailtime;// = new Timestamp(System.currentTimeMillis());
	private BigDecimal price;
	private BigDecimal totalProduct;
	private Double quantity;
	private BigDecimal taxAmount;
	private BigDecimal totalAjdustment;
	public Long getOrderitemId() {
		return orderitemId;
	}
	public void setOrderitemId(Long orderitemId) {
		this.orderitemId = orderitemId;
	}
	public Orders getOrders() {
		return orders;
	}
	public void setOrders(Orders orders) {
		this.orders = orders;
	}
//	public Long getMenuItemId() {
//		return menuItemId;
//	}
//	public void setMenuItemId(Long menuItemId) {
//		this.menuItemId = menuItemId;
//	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Timestamp getTimePlaced() {
		return timePlaced;
	}
	public void setTimePlaced(Timestamp timePlaced) {
		this.timePlaced = timePlaced;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Timestamp getEstAvailtime() {
		return estAvailtime;
	}
	public void setEstAvailtime(Timestamp estAvailtime) {
		this.estAvailtime = estAvailtime;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getTotalProduct() {
		return totalProduct;
	}
	public void setTotalProduct(BigDecimal totalProduct) {
		this.totalProduct = totalProduct;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public BigDecimal getTotalAjdustment() {
		return totalAjdustment;
	}
	public void setTotalAjdustment(BigDecimal totalAjdustment) {
		this.totalAjdustment = totalAjdustment;
	}
	
}
