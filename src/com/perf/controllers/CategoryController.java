package com.perf.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.model.products.Category;
import com.perf.service.products.CategoryService;

@RestController
@RequestMapping("/category")
@Transactional
public class CategoryController {
	@Autowired
	CategoryService categoryService;

	@PostMapping("/create") 
	public Category addCategory(
			@RequestParam Map<String, String> requestMap) {
		return categoryService.addCategory(requestMap);
	}
	
	@PutMapping("/update/{categoryId}")
	public Object updateCategory(
			@RequestParam Map<String, String> requestMap, 
			@PathVariable Long categoryId) {
		return categoryService.updateCategory(categoryId, requestMap);
	}
	
	@DeleteMapping("/delete/{categoryId}")
	public boolean deleteCategory(
			@PathVariable Long categoryId) {
		return categoryService.deleteCategory(categoryId);
	}
	
	@GetMapping("/find/{categoryId}")
	public Object findCategory(
			@RequestParam Map<String, String> requestMap,
			@PathVariable Long categoryId) {
		return categoryService.findCategoryById(categoryId);
		 
	}
	
	@GetMapping("/find/name/{name}")
	public Object findCategoryByName(
		 
			@PathVariable String name) {
		return categoryService.findCategoryByName(name);
		 
	}
	@GetMapping("/find/name/detail/{name}")
	public Object findCategoryWithDetailsByName(
		 
			@PathVariable String name) {
		return categoryService.findCategoryWithDetailsByName(name);
		 
	}
	
	@GetMapping("/find-all")
	public Object findAllCategorys() {
		return categoryService.findAll();
		
	}
	
}
