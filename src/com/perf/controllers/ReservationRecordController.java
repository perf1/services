package com.perf.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.model.reservation.ReservationRecord;
import com.perf.service.reservation.ReservationRecordService;

@RestController
@RequestMapping("/reservationRecord")
@Transactional
public class ReservationRecordController {
	@Autowired
	ReservationRecordService reservationRecordService;
	
	@RequestMapping("create")
	public Long createReservationRecord(@RequestParam Map<String, String> requestMap, @RequestBody(required=false) String str) {
		return reservationRecordService.createRecord(requestMap);
	}
	
	@GetMapping("find/{id}")
	//@DTOConverter(to = UserDTO.class, ignore = { "password" })
	ReservationRecord find(@RequestParam Map<String, String> requestMap, @PathVariable Long id) {
		Optional<ReservationRecord> t = reservationRecordService.findReservationRecord(id);
		return t.isPresent() ? t.get() : null;
	}
	
	@GetMapping("find-all")
	//@DTOConverter(to = UserDTO.class, ignore = { "password" })
	List<ReservationRecord> all() {
		return reservationRecordService.findAllReservationRecord();
	}
	
	@DeleteMapping("delete/{id}")
	//@DTOConverter(to = UserDTO.class, ignore = { "password" })
	public void deleteReservationRecord(@RequestParam Map<String, String> requestMap, @PathVariable Long id) {
		reservationRecordService.deleteReservationRecord(id);
	}
	
	@PostMapping("update")
	public void updateReservationRecord(@RequestParam Map<String, String> requestMap, @PathVariable Long id) {
		
		reservationRecordService.updateReservationRecord(requestMap);
	}
}
