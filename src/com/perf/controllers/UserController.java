package com.perf.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.model.users.Address;
import com.perf.model.users.User;
import com.perf.service.users.AddressService;
import com.perf.service.users.UserService;

@RestController
@RequestMapping("/user")
@Transactional
@CacheConfig(cacheNames = "userCache")
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	AddressService addressService;

	@GetMapping("/find-all")
	List<User> all() {
		return userService.findAll();
	}

	@RequestMapping("find/{uid}")
	@Cacheable(key = "#uid")
	// @PostAuthorize("hasPermission(returnObject,'READ')")
	@PreAuthorize("#uid == principal.userId or hasRole('ADMIN')")
	public Object findUser(@RequestParam Map<String, String> requestMap, @PathVariable Long uid) {
		System.out.println("find uid" + uid);
		User u = userService.getUser(uid).orElse(null);
		Address address = null;
		if (u != null) {
			address = addressService.findProfileAddressByUserId(u.getUserId());
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("users", u);
		if (address != null)
			map.put("address", address);
		return map;
	}

	@DeleteMapping("delete/{uid}")
	@CacheEvict(key = "#uid")
	@PreAuthorize("#uid == principal.userId or hasRole('ADMIN')")
	public void deleteUser(@RequestParam Map<String, String> requestMap, @PathVariable Long uid) {
		int result = userService.deleteUser(uid);

	}

}
