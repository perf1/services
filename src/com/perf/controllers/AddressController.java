package com.perf.controllers;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.model.users.Address;
import com.perf.security.rest.MyUserPrincipal;
import com.perf.service.users.AddressService;

@RestController
@Transactional
@CacheConfig (cacheNames="addressCache")
@RequestMapping("/address")
public class AddressController {

	@Autowired
	AddressService addressService;
	
	@PostMapping ("/add")
	public Address addAddress(@RequestParam Map requestMap) {
		MyUserPrincipal p = (MyUserPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return addressService.addAddress(requestMap,p.getUserId());
	}
	@PostMapping ("/update/{addressId}")
	public Address addAddress(@RequestParam Map requestMap, Long addressId) {
		return addressService.updateAddress(requestMap, addressId);
	}
	
	@PostMapping ("/find/{addressId}")
	@PostAuthorize("hasPermission(returnObject,'READ')")
	public Address findAddressById(@PathVariable Long addressId) {
		return addressService.findById(addressId);
	}
	
	@PostMapping ("/find-by-user-nickname/{userId}/{nickName}")
	public Address findAddressByUserAndNickName(@PathVariable Long userId,@PathVariable String nickName) {
		return addressService.findByUserIdAndNickName(userId, nickName);
	}
	
	@PostMapping ("/find-by-user/{userId}")
	public List<Address> findAddressByUserAndNickName(@PathVariable Long userId ) {
		return addressService.findAllByUserId(userId);
	}
	
	@PostMapping ("/delete/{addressId}")
	public boolean delete(@PathVariable Long addressId ) {
		return addressService.deleteAddressById(addressId) ;
	}
	
	
	
}
