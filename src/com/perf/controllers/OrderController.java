package com.perf.controllers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.model.order.OrderItem;
import com.perf.model.order.Orders;
import com.perf.service.order.OrderItemService;
import com.perf.service.order.OrderService;

@RestController
@RequestMapping("/order")
@Transactional
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	@PostMapping("/createOrder")
	public Long createOrder(@RequestParam Map<String, String> requestMap) {
		return orderService.createOrder(requestMap);
	}
	
	@GetMapping("/findOrderById/{orderId}")
	//@DTOConverter(to = UserDTO.class, ignore = { "password" })
	public Object findOrderById(@RequestParam Map<String, String> requestMap, @PathVariable Long orderId) {
		Optional<Orders> order = orderService.getOrderByOrdersId(orderId);
		return order.isPresent()?order.get():null;
	}
	
	@GetMapping("/calculateOrder/{orderId}")
	public Object calculateOrder(@PathVariable Long orderId) {		
		return orderService.orderCalculate(orderId);
	}

	@Autowired
	OrderItemService orderItemService;
	
	@PostMapping("/addItem")
	public Long addItem(@RequestParam Map<String, String> requestMap) {
		return orderItemService.addItem(requestMap);
	}
	
	@PostMapping("/updateItem/{orderItemId}")
	public Long updateItem(@RequestParam Map<String, String> requestMap, @PathVariable Long orderItemId) {
		return orderItemService.updateItem(requestMap, orderItemId);
	}
	
	@PostMapping("/deleteItem/{orderItemId}")
	public int deleteItem(@PathVariable Long orderItemId) {
		return orderItemService.deleteItem(orderItemId);
	}
	
	@GetMapping("/findItemByOrder/{orderId}")
	public List<OrderItem> findItemByOrder(@RequestParam Map<String, String> requestMap, @PathVariable Long orderId) {
		return orderItemService.getOrderItemsByOrderId(orderId);
	}
}
