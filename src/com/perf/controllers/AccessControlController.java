package com.perf.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perf.model.security.Role;
//import com.perf.service.security.AclService;

@RestController
@RequestMapping("/admin/access-control")
@CacheConfig(cacheNames="aclCache")
public class AccessControlController {
	@Autowired
//	AclService aclService;
	
	@Cacheable(keyGenerator="cacheKeyGenerator")
	@RequestMapping("/roles/find-all")
	public Collection<Role> getAllRoles(){
//		return aclService.getAllRoles();
		return null;
	}
}
