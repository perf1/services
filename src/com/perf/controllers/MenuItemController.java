package com.perf.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.model.menuItems.MenuItem;
import com.perf.service.menuItems.MenuItemService;

@RestController
@RequestMapping("/menuitem")
@Transactional
public class MenuItemController {
	@Autowired
	MenuItemService menuItemService;

	@PostMapping("/create") 
	public MenuItem addMenuItem(
			@RequestParam Map<String, String> requestMap) {
		return menuItemService.addMenuItem(requestMap);
	}
	
	@PutMapping("/update/{menuItemId}")
	public boolean updateMenuItem(
			@RequestParam Map<String, String> requestMap, 
			@PathVariable Long menuItemId) {
		return menuItemService.updateMenuItem(menuItemId, requestMap);
	}
	
	@DeleteMapping("/delete/{menuItemId}")
	public boolean deleteMenuItem(
			@PathVariable Long menuItemId) {
		return menuItemService.deleteMenuItem(menuItemId);
	}
	
	@GetMapping("/find/{menuItemId}")
	public MenuItem findMenuItem(
			@RequestParam Map<String, String> requestMap,
			@PathVariable Long menuItemId) {
		Optional<MenuItem> mi = menuItemService.findMenuItemById(menuItemId);
		
		return mi.isPresent() ? mi.get() : null;
	}
	
	@GetMapping("/find-all")
	public List<MenuItem> findAllMenuItems() {
		Object t=menuItemService.findAll();
		return menuItemService.findAll();
	}
	
}
