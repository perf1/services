package com.perf.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.base.annotation.DTOConverter;
import com.perf.dto.users.UserDTO;
import com.perf.model.reservation.ReserveTable;
import com.perf.model.users.User;
import com.perf.service.reservation.ReserveTableService;

@RestController
@RequestMapping("/reserveTable")
@Transactional
public class ReserveTableController {
	
	@Autowired
	ReserveTableService reserveTableService;
	
	@GetMapping("/find-all")
	//@DTOConverter(to = UserDTO.class, ignore = { "password" })
	List<ReserveTable> all() {
		return reserveTableService.findAllReserveTable();
	}

	@RequestMapping("find/{id}")
	// @PreAuthorize("#uid == authentication.userId")
	//@DTOConverter(to = UserDTO.class, ignore = { "password" })
	public Object findReserveTable(@RequestParam Map<String, String> requestMap, @PathVariable String id) {
		Optional<ReserveTable> t = reserveTableService.getReserveTable(id);
		return t.isPresent() ? t.get() : null;

	}

	@DeleteMapping("delete/{id}")
	//@DTOConverter(to = UserDTO.class, ignore = { "password" })
	public void deleteReserveTable(@RequestParam Map<String, String> requestMap, @PathVariable String id) {
		int result = reserveTableService.deleteReserveTable(id);
	}
	
	@RequestMapping("add")
	public Long addReserveTable(@RequestParam Map<String, String> requestMap, @RequestBody(required=false) String str) {
		return reserveTableService.addReserveTable(requestMap);
	}
	
	//public void updateReserveTable()

}
