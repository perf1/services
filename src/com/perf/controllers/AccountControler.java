package com.perf.controllers;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.perf.base.annotation.DTOConverter;
import com.perf.base.annotation.RequestValidation;
import com.perf.base.exception.BaseException;
import com.perf.constant.SecurityConstants;
import com.perf.dto.users.UserDTO;
import com.perf.model.users.User;
import com.perf.security.rest.JwtTokenService;
import com.perf.service.users.UserService;

@RestController
@RequestMapping("/account")
@Transactional
@CacheConfig(cacheNames="userCache")
public class AccountControler {
	@Autowired
	UserService userService;

	@PostMapping("/login")
//	@DTOConverter(to = UserDTO.class, ignore = { "password" })
//	@DTOConverter( ignore = { "password" })
	public Object login(@RequestParam String logonId, @RequestParam String password, HttpServletResponse response) throws Exception {

		User user = userService.logon(logonId, password);
		// verify valid user
		validateUser(user);
		generateJWTAndSetSecurityContext(user, response, userService.getGrantedAuthorities(user));
		return user;
	}

	private void validateUser(User user) throws Exception{
		// is user enabled
		
		// is user account expired or locked
		
		// is user password expired
		
		 
	}

	@Autowired
	JwtTokenService jwtTokenService;

	@RequestMapping("/register")
//	@CachePut(key="#result.userId")
	public Object registerUser(@RequestParam Map<String, String> requestMap, HttpServletResponse response) throws BaseException {
		User user = userService.registerUser(requestMap);
		if(user!=null) {
			// TODO: Generate and set user default authority and role
			Collection<? extends GrantedAuthority> authorities = userService.getDefaultAuthority();
			
			generateJWTAndSetSecurityContext(user, response, authorities);
		}
		return user;

	}
	@RequestMapping("/update/{userId}")
	@CacheEvict(key="#result.userId")
	@PreAuthorize("#userId = principal.userId or hasRole('ADMIN')")
	@RequestValidation(required= {"logonId"})
	public Object updateUser(@RequestParam Map<String, String> requestMap, HttpServletResponse response, @PathVariable Long userId) throws BaseException {
		User user = userService.updateUser(requestMap, userId);
		 
		return user;

	}
	
	private void generateJWTAndSetSecurityContext(User user, HttpServletResponse response, Collection<? extends GrantedAuthority> authorities) {
		if (user != null) {		 
			String token = jwtTokenService.createJwtToken(user, authorities);
			response.addHeader(SecurityConstants.AUTHORIZATION,
					SecurityConstants.JWT_BEARER_TOKEN_PREFIX + " " + token);
		}
	}
}
