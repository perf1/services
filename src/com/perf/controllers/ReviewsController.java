package com.perf.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.perf.base.annotation.DTOConverter;
import com.perf.dto.reviews.ReviewsDTO;
import com.perf.service.reviews.ReviewService;

@RestController
@RequestMapping("/reviews")
@Transactional
public class ReviewsController {
	@Autowired
	ReviewService reviewService;
	
	@PostMapping("/create")
	@DTOConverter(to = ReviewsDTO.class)
	public ReviewsDTO addReview(@RequestParam String userId, @RequestParam String menuItemId, @RequestParam String message, HttpServletResponse response) throws Exception {		
		return reviewService.addReview(menuItemId, userId, message);	
	}
   
	@GetMapping("/find/reviewByMenuItemId/{menuItemId}")
	public List<ReviewsDTO> findReviewsByMenuItemId(
			@RequestParam Map<String, String> requestMap,
			@PathVariable Long menuItemId){
		return reviewService.findAllReviewsByMenuItemId(menuItemId); 
	}
	
}
