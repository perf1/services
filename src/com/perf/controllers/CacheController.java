package com.perf.controllers;

import java.sql.Timestamp;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cache")
public class CacheController {
	@RequestMapping("/clear-all")
	@CacheEvict(allEntries=true,cacheNames= {"userCache","globalCache"})
	public Object clearAll() {
		return getMessage("all caches ");
	}
//	@RequestMapping("/clear/{cacheName}")
//	@CacheEvict(allEntries=true,cacheNames= {"#cacheName" })
//	public Object clearCache(@PathVariable String cacheName) {
//		return getMessage(cacheName);
//	}
	@RequestMapping("/clear/user-cache")
	@CacheEvict(allEntries=true,cacheNames= {"userCache" })
	public Object clearUserCache() {
		return getMessage("userCache");
	}
	
	@RequestMapping("/clear/global-cache")
	@CacheEvict(allEntries=true,cacheNames= {"globalCache" })
	public Object clearGlobalCache() {
		return getMessage("globalCache");
	}
	
	private Object getMessage(String entryName) {
		return entryName + " is cleared at " +  new Timestamp(System.currentTimeMillis());
	}
	
}
