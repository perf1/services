package com.perf.service.products;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.perf.dto.items.CategoryDTO;
import com.perf.dto.items.ItemDTOUtil;
import com.perf.model.products.Category;
import com.perf.repository.menuItems.MenuItemRepository;
import com.perf.repository.products.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	CategoryRepository categoryRepositoy;
	@Autowired
	MenuItemRepository menuItemRepositoy;
	@Autowired
	ItemDTOUtil itemDTOUtil;
	@Override
	public Category addCategory(Map<String, String> categoryData) {
		Category category = new Category();
		category.setName((String) categoryData.get("name"));
		category.setDescription((String)categoryData.get("description"));
		if(categoryData.get("parent")!=null) {
			Optional<Category> parentCat = categoryRepositoy.findByName(categoryData.get("parent").toString());
			if(parentCat.isPresent()) {
				Set<Category> set = new HashSet<Category>();
				set.add(parentCat.get());
				category.setParentCategories(set);
			}
		}
		categoryRepositoy.save(category);
		
		return category;
	}

	@Override
	public Object updateCategory(Long categoryId, Map<String, String> categoryData) {
		boolean isUpdated = false;

		Optional<Category> category = categoryRepositoy.findById(categoryId);

		if (category.isPresent()) {
			Category newCategory = category.get();
			String name = (String) categoryData.get("name");
			String desc = (String) categoryData.get("description");
			newCategory.setName(name);
			newCategory.setDescription(desc);
			 
			categoryRepositoy.save(newCategory); 
			return newCategory;
		}  

		return null;
	}

	@Override
	public boolean deleteCategory(Long id) {
		boolean isDeleted = false;
		try {
			categoryRepositoy.deleteById(id);
			isDeleted = true;
		} catch (Exception e) {
			// delete failed
		}

		return isDeleted;
	}

	@Override
	public Category findCategoryById(Long id) {
		 
		Optional<Category> categoryOpt =  categoryRepositoy.findById(id);
		
		 if(categoryOpt.isPresent()) {
			 return categoryOpt.get();
		 }
		return null;
	}

	@Override
	public List<Category> findAll() {

		List<Category> categories =  categoryRepositoy.findAll();
		
		
		 
		return categories; 
		
	}

	@Override
	public Category findCategoryByName(String name) {
		Optional<Category> categoryOpt =  categoryRepositoy.findByName(name);
		
		 if(categoryOpt.isPresent()) {
			 return categoryOpt.get();
		 }
		return null;
	}

	@Override
	public Object findCategoryWithDetailsByName(String name) {
		Category category = findCategoryByName(name);
		if(category == null) return null;
		
		CategoryDTO categoryDTO =itemDTOUtil.convertDTO(category);
		return categoryDTO;
	}
}
