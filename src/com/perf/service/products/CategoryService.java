package com.perf.service.products;

import java.util.List;
import java.util.Map;

import com.perf.model.products.Category;

public interface CategoryService {

	Category addCategory(Map<String, String> categoryData);

	Object updateCategory(Long categoryId, Map<String, String> categoryData);

	boolean deleteCategory(Long id);

	 Category findCategoryById(Long id);

	List<Category> findAll();

	Category findCategoryByName(String name);

	Object findCategoryWithDetailsByName(String name);

}
