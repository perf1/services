package com.perf.service.reservation;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.perf.model.reservation.ReservationRecord;
import com.perf.model.reservation.ReserveTable;
import com.perf.repository.reservation.ReservationRecordRepository;

@Service
public class ReservationRecordServiceImpl implements ReservationRecordService {
	@Autowired
	ReservationRecordRepository reservationRecordRepository;

	@Override
	public Long createRecord(Map data) {
		System.out.println("data:"+data);
		ReservationRecord r = new ReservationRecord();
		r.setCustomerName((String) data.get("name"));
		r.setSeats(Integer.valueOf((String) data.get("seats")));
		r.setContactInfo((String) data.get("contact"));
		r.setSpecialRequest((String) data.get("specialRequest"));
		r.setStartTime((String) data.get("time"));
		reservationRecordRepository.save(r);
		return r.getReservationId();
	}

	@Override
	public Optional<ReservationRecord> findReservationRecord(Long id) {		
		return reservationRecordRepository.findById(id);
	}

	@Override
	public ReserveTable findAvailableTable(Map data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateReservationRecord(Map data) {
		Optional<ReservationRecord> r = reservationRecordRepository.findById((Long) data.get("id"));
		if(r.isPresent()) {
			ReservationRecord thisRecord = r.get();
			if(data.containsKey("name"))
				thisRecord.setCustomerName((String) data.get("name"));
			if(data.containsKey("seats"))
				thisRecord.setSeats((int) data.get("seats"));
			if(data.containsKey("contact"))
				thisRecord.setContactInfo((String) data.get("contact"));
			if(data.containsKey("specialRequest"))
				thisRecord.setSpecialRequest((String) data.get("specialRequest"));
			if(data.containsKey("time"))
				thisRecord.setStartTime((String) data.get("time"));
			
			reservationRecordRepository.save(thisRecord);
		}
	}

	@Override
	public void deleteReservationRecord(Long id) {
		reservationRecordRepository.deleteById(id);		
	}

	@Override
	public List<ReserveTable> findAvailableReserveTable(Map data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ReservationRecord> findReservationRecord(Map data) {
		
		ReservationRecord r = new ReservationRecord();
		r.setCustomerName((String) data.get("name"));
		r.setSeats((int) data.get("seats"));
		r.setContactInfo((String) data.get("contact"));
		r.setSpecialRequest((String) data.get("specialRequest"));
		r.setStartTime((String) data.get("time"));
		
		Example<ReservationRecord> example = Example.of(r);
		
		return reservationRecordRepository.findAll(example);
	}

	@Override
	public List<ReservationRecord> findAllReservationRecord() {
		// TODO Auto-generated method stub
		return reservationRecordRepository.findAll();
	}

}
