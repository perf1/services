package com.perf.service.reservation;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.perf.model.reservation.ReserveTable;
import com.perf.repository.reservation.ReserveTableRepository;

@Service
public class ReserveTableServiceImpl implements ReserveTableService {
	@Autowired
	ReserveTableRepository reserveTableRepository;
	
	@Override
	public Long addReserveTable(Map data) {
		ReserveTable t = new ReserveTable();
		t.setTableType((String) data.get("type"));
		t.setSeats((int) data.get("seats"));
		reserveTableRepository.save(t);
		
		return t.getTableId();
	}

	@Override
	public int deleteReserveTable(String id) {
		try {
			reserveTableRepository.deleteById(Long.valueOf(id));
		} catch (Exception e) {
			return -1;
		}
		return 0;
	}

	@Override
	public Optional<ReserveTable> getReserveTable(String id) {
		return reserveTableRepository.findById(Long.valueOf(id));
	}

	@Override
	public void updateReserveTable(Map data) {
		Optional<ReserveTable> t = reserveTableRepository.findById((Long) data.get("id"));
		if(t.isPresent()) {
			ReserveTable thisTbl = t.get();
			thisTbl.setTableType((String) data.get("type"));
			reserveTableRepository.save(thisTbl);
		}
	}

	@Override
	public List<ReserveTable> findAllReserveTable() {
		return reserveTableRepository.findAll();
	}

}
