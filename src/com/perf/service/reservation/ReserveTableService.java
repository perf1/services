package com.perf.service.reservation;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.perf.model.reservation.ReserveTable;

public interface ReserveTableService {
	Long addReserveTable(Map data);
	
	int deleteReserveTable(String id);

	Optional<ReserveTable> getReserveTable(String id);

	void updateReserveTable(Map data);
	
	List<ReserveTable> findAllReserveTable();
}
