package com.perf.service.reservation;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.perf.model.reservation.ReservationRecord;
import com.perf.model.reservation.ReserveTable;

public interface ReservationRecordService {
	Long createRecord(Map data);
	
	List<ReservationRecord> findReservationRecord(Map data);
	
	Optional<ReservationRecord> findReservationRecord(Long id);
	
	List<ReservationRecord> findAllReservationRecord();
	
	void updateReservationRecord(Map data);
	
	void deleteReservationRecord(Long id);
	
	List<ReserveTable> findAvailableReserveTable(Map data);
	
	ReserveTable findAvailableTable(Map data);
}
