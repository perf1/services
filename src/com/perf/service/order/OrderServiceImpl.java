package com.perf.service.order;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.perf.model.order.OrderItem;
import com.perf.model.order.Orders;
import com.perf.repository.order.OrderItemRepository;
import com.perf.repository.order.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	OrderItemRepository orderItemRepository;

	@Override
	public Long createOrder(Map orderData) {
		// TODO Auto-generated method stub
		Orders order = new Orders();
		order.setStatus("P");
		return orderRepository.save(order).getOrdersId();
	}

	@Override
	public Long getCurrentPendingOrder(String logonId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Orders> getOrdersByUserId(String logonId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Orders> getOrderByOrdersId(Long orderId) {
		// TODO Auto-generated method stub
		return orderRepository.findById(orderId);
	}

	@Override
	public Orders orderCalculate(Long orderId) {
		// TODO Auto-generated method stub
		Orders orderObj = null;
		Optional<Orders> order = orderRepository.findById(orderId);
		List<OrderItem> oi = orderItemRepository.findByOrdersId(orderId);
		if(order.isPresent() && !oi.isEmpty()) {
			Iterator<OrderItem> it = oi.iterator();
			BigDecimal orderTax = new BigDecimal(0);
			BigDecimal orderAdjustment = new BigDecimal(0);
			BigDecimal orderTotal = new BigDecimal(0);
			while(it.hasNext()) {
				OrderItem item = it.next();
				//item.getPrice();
				//item.getQuantity();
				
				if(item.getTaxAmount() !=null) orderTax = orderTax.add(item.getTaxAmount());
				if(item.getTotalAjdustment() !=null) orderAdjustment = orderAdjustment.add(item.getTotalAjdustment());
				if(item.getTotalProduct() !=null) orderTotal = orderTotal.add(item.getTotalProduct());
			}			
			orderObj = order.get();
			orderObj.setTotalProduct(orderTotal.add(orderAdjustment));
			orderObj.setTotalTax(orderTax);
			orderRepository.save(orderObj);
		}
		return orderObj;
	}	

}
