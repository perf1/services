package com.perf.service.order;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.perf.model.order.OrderItem;
import com.perf.model.order.Orders;
import com.perf.model.users.User;
import com.perf.repository.order.OrderItemRepository;
import com.perf.repository.order.OrderRepository;
import com.perf.repository.users.UserRepository;

@Service
public class OrderItemServiceImpl implements OrderItemService {
	
	@Autowired
	OrderItemRepository orderItemRepository;
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	UserRepository userRepository;

	@Override
	public int deleteItem(Long orderItemId) {
		// TODO Auto-generated method stub
		orderItemRepository.deleteById(orderItemId);
		return 0;
	}

	@Override
	public Long addItem(Map itemData) {
		// TODO Auto-generated method stub
		OrderItem oi = new OrderItem();
		oi.setPrice(new BigDecimal(itemData.get("price").toString()));
		oi.setQuantity(new Double(itemData.get("quantity").toString()));
		Optional<Orders> order = orderRepository.findById(new Long(itemData.get("orderId").toString()));
		if(order.isPresent()) {
			oi.setOrders(order.get());
		}else {
			//TODO create new order
			Orders o = new Orders();
			oi.setOrders(orderRepository.save(o));
		}
		Optional<User> user = userRepository.findByLogonId(itemData.get("logonId").toString());
		if(user.isPresent()) {
			oi.setUser(user.get());
		}else {
			oi.setUser(null);
		}
		//oi.setMenuItemId(new Long(itemData.get("menuItemId").toString()));
		calculateItemTotal(oi);
		return orderItemRepository.save(oi).getOrderitemId();
	}
	
	@Override
	public Long updateItem(Map itemData, Long orderItemId) {
		// TODO Auto-generated method stub
		Optional<OrderItem> oi = orderItemRepository.findById(orderItemId);
		Long result = null;
		if(oi.isPresent()) {
			OrderItem item = oi.get();
			if(itemData != null) {
				if(itemData.get("price") != null) {
					item.setPrice(new BigDecimal(itemData.get("price").toString()));
				}
				if(itemData.get("quantity") != null) {
					item.setQuantity(new Double(itemData.get("quantity").toString()));
				}
			}						
			item.setLastUpdate(new Timestamp(System.currentTimeMillis()));
			//oi.setMenuItemId(new Long(itemData.get("menuItemId").toString()));
			calculateItemTotal(item);
			result = orderItemRepository.save(item).getOrderitemId();
		}
		
		return result;
	}


	@Override
	public List<OrderItem> getOrderItemsByOrderId(Long order_id) {
		// TODO Auto-generated method stub
		List<OrderItem> results = null;
		Optional<Orders> order = orderRepository.findById(order_id);
		if(order.isPresent()) {
			List<OrderItem> oi = orderItemRepository.findByOrdersId(order_id);
			if(oi!=null && oi.size() > 0) {
				results = oi;
			}
		}
		return results;
	}
	
	private void calculateItemTotal(OrderItem oi) {
		oi.setTotalProduct(oi.getPrice().multiply(new BigDecimal(oi.getQuantity())));
	}

}
