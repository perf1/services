package com.perf.service.order;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.perf.model.order.Orders;

public interface OrderService {
	
	Long createOrder(Map orderData);
	
	Long getCurrentPendingOrder(String logonId);
	
	List<Orders> getOrdersByUserId(String logonId);
	
	Optional<Orders> getOrderByOrdersId(Long orderId);
	
	Orders orderCalculate(Long orderId);

}
