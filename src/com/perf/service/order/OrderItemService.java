package com.perf.service.order;

import java.util.List;
import java.util.Map;

import com.perf.model.order.OrderItem;

public interface OrderItemService {
	
	int deleteItem(Long orderItemId);
	
	Long addItem(Map itemData);
	
	Long updateItem(Map itemData, Long orderItemId);
	
	List<OrderItem> getOrderItemsByOrderId(Long orderId);
	

}
