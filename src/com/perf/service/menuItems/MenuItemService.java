package com.perf.service.menuItems;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.perf.model.menuItems.MenuItem;

public interface MenuItemService {

	MenuItem addMenuItem(Map<String, String> menuItemData);

	boolean updateMenuItem(Long id, Map<String, String> menuItemData);

	boolean deleteMenuItem(Long id);

	Optional<MenuItem> findMenuItemById(Long id);
	
	List<MenuItem> findAll();


}
