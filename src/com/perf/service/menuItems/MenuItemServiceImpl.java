package com.perf.service.menuItems;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.perf.model.menuItems.MenuItem;
import com.perf.repository.menuItems.MenuItemRepository;

@Service
public class MenuItemServiceImpl implements MenuItemService {
	@Autowired
	MenuItemRepository menuItemRepositoy;
	
	@Override
	public MenuItem addMenuItem(Map<String, String> menuItemData) {
		MenuItem mi = new MenuItem();
		mi.setMenuItemName((String) menuItemData.get("menuItemName"));
		mi.setMenuItemDesc((String) menuItemData.get("menuItemDesc"));
		menuItemRepositoy.save(mi);
		
		return mi;
	}
	
	@Override
	public boolean updateMenuItem(Long menuItemId, Map<String, String> menuItemData) {
		boolean isUpdated = false;
				
		Optional<MenuItem> menuItem = menuItemRepositoy.findById(menuItemId);
		
		if(menuItem.isPresent()) {
			MenuItem newMenuItem = menuItem.get();
			String name = (String) menuItemData.get("menuItemName");
			String desc = (String) menuItemData.get("menuItemDesc");
			newMenuItem.setMenuItemName(name);
			newMenuItem.setMenuItemDesc(desc);
			newMenuItem.setLastUpdate(new Timestamp(System.currentTimeMillis()));
			menuItemRepositoy.save(newMenuItem);
			
			isUpdated = true;
		} else {
			// update failed
		}
		
		return isUpdated;
	}
	
	@Override
	public boolean deleteMenuItem(Long id) {
		boolean isDeleted = false;
		try {
			menuItemRepositoy.deleteById(id);
			isDeleted = true;
		} catch (Exception e) {
			// delete failed
		}
		
		return isDeleted;
	}

	@Override
	public Optional<MenuItem> findMenuItemById(Long id) {
		
		return menuItemRepositoy.findById(id);
	}
	
	@Override
	public List<MenuItem> findAll() {
		
		return menuItemRepositoy.findAll();
	}
}
