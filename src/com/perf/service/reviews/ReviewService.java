package com.perf.service.reviews;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.perf.dto.reviews.ReviewsDTO;
import com.perf.model.reviews.Reviews;

public interface ReviewService {
	public ReviewsDTO addReview(String menuItemId, String userId, String message);
	public List<ReviewsDTO> findAllReviewsByMenuItemId(Long menuItemId); 
} 