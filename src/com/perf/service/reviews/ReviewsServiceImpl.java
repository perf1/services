package com.perf.service.reviews;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.perf.dto.reviews.ReviewsDTO;
import com.perf.model.menuItems.MenuItem;
import com.perf.model.reviews.Reviews;
import com.perf.model.users.User;
import com.perf.repository.menuItems.MenuItemRepository;
import com.perf.repository.reviews.ReviewsRepository;
import com.perf.repository.users.UserRepository;

@Service
public class ReviewsServiceImpl implements ReviewService {
	@Autowired
	ReviewsRepository reviewsRepository; 
	
	@Autowired
	MenuItemRepository menuItemRepository; 
	
	@Autowired
	UserRepository userRepository; 
	
	@Override
	public ReviewsDTO addReview(String menuItemId, String userId, String message){
		 
		Timestamp ts = new Timestamp(System.currentTimeMillis());		
		Reviews reviews = new Reviews(); 
		
		Optional<MenuItem> menuItem = menuItemRepository.findById(Long.valueOf(menuItemId));
		Optional<User> user = userRepository.findById(Long.valueOf(userId)); 
		
		if (menuItem.isPresent() || user.isPresent()) {
			reviews.setMenuItem(menuItem.get());
			reviews.setUser(user.get());
		} else {
			//throw da exception 
		}
		
		reviews.setMessage(message);
		reviews.setTimestamp(ts);
		
		reviewsRepository.save(reviews);
		
		
		//Format Response
		ReviewsDTO response = new ReviewsDTO(); 
		response.setMessage(reviews.getMessage());
		response.setUsername(reviews.getUser().getLogonId());
		response.setReviewId(reviews.getReviewId().toString());
		
		return response; 
	}

	@Override
	public List<ReviewsDTO> findAllReviewsByMenuItemId(Long menuItemId) {
		Optional<MenuItem> menuItem = menuItemRepository.findById(menuItemId);
		
		
		List<Reviews> reviewList = new ArrayList<Reviews>(menuItem.get().getReviews());
		List <ReviewsDTO> response = new ArrayList<ReviewsDTO>();
		for (Reviews review: reviewList) {
			ReviewsDTO reviewRecord = new ReviewsDTO(); 
			reviewRecord.setMessage(review.getMessage());
			reviewRecord.setMenuItemId(review.getMenuItem().getMenuItemId().toString());
			reviewRecord.setUsername(review.getUser().getLogonId());
			reviewRecord.setUserId(review.getUser().getUserId().toString());
			reviewRecord.setReviewId(review.getReviewId().toString());
			reviewRecord.setDisplayDate(review.getTimestamp().getTime());
			response.add(reviewRecord);
		}
		
		return response; 
	}
	
}
