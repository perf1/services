package com.perf.service.users;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.perf.constant.AddressConstants;
import com.perf.model.users.Address;
import com.perf.model.users.User;
import com.perf.repository.users.AddressRepository;
import com.perf.repository.users.UserRepository;
import com.perf.utils.RequestHelper;

@Service
public class AddressServiceImpl implements AddressService {
	@Autowired
	AddressRepository addressRepository;
	@Autowired
	UserRepository userRepository;
	
	/* (non-Javadoc)
	 * @see com.perf.service.users.AddressService#addAddress(java.util.Map)
	 */
	@Override
	public Address addAddress(Map addressData, Long userId) {
		Address address =  new Address();
		address.setStatus(AddressConstants.STATUS_ACTIVE);
		address.setUser(userRepository.findById(userId).get());
		return this.addOrUpdateAddress(  addressData, address);
	}
	
	/* (non-Javadoc)
	 * @see com.perf.service.users.AddressService#updateAddress(java.util.Map)
	 */
	@Override
	public Address updateAddress(Map addressData, Long addressId) {
		Address address = null;
		if(addressId!=null) {
			address = addressRepository.findById(addressId).orElse(null);			
		}
		if(address == null) {
			address = new Address();
			address.setStatus(AddressConstants.STATUS_ACTIVE);
		}
		return this.addOrUpdateAddress(  addressData, address);
	}
	
	/* (non-Javadoc)
	 * @see com.perf.service.users.AddressService#addOrUpdateAddress(java.util.Map)
	 */
	 
	private  Address addOrUpdateAddress(Map addressData, Address address) {
		
		RequestHelper requestHelper = new RequestHelper(addressData);
		address.setAddress1(requestHelper.getString( "address1"));
		address.setAddress2(requestHelper.getString( "address2"));
		address.setEmail(requestHelper.getString( "email"));
		
		address.setFirstName(requestHelper.getString( "firstName"));
		address.setLastName(requestHelper.getString( "lastName"));
		address.setCity(requestHelper.getString( "city"));
		address.setProvince(requestHelper.getString( "province"));
		address.setCountry(requestHelper.getString( "country"));
		address.setPostalCode(requestHelper.getString( "postalCode"));
		address.setNickName(requestHelper.getString( "nickName"));
		if(requestHelper.getBoolean("isProfile")) {
			address.setProfile(true);
		}
		address.setLastUpdate(new Timestamp(System.currentTimeMillis()));
		addressRepository.save(address);
		return address;
	}
	
	/* (non-Javadoc)
	 * @see com.perf.service.users.AddressService#findById(java.lang.Long)
	 */
	@Override
	public Address findById (Long addressId) {
		return addressRepository.findById(addressId).orElse(null);
	}
	
	/* (non-Javadoc)
	 * @see com.perf.service.users.AddressService#findByUserIdAndNickName(java.lang.Long, java.lang.String)
	 */
	@Override
	public Address findByUserIdAndNickName (Long userId, String nickName) {
		User user = userRepository.findById(userId).orElse(null);
		if(user!=null) {
			return addressRepository.findByUserIdAndNickName(user.getUserId(), nickName);
		}else {
			return null;
		}
		
	}
	/* (non-Javadoc)
	 * @see com.perf.service.users.AddressService#findByUserId(java.lang.Long)
	 */
	@Override
	public List<Address> findAllByUserId  (Long userId ) {
		 
			return addressRepository.findAllByUserId(userId); 
		
	}
	/* (non-Javadoc)
	 * @see com.perf.service.users.AddressService#deleteAddressById(java.lang.Long)
	 */
	@Override
	public boolean deleteAddressById (Long addressId) {
		addressRepository.deleteById(addressId);
		return true;
	}

	@Override
	public Address findProfileAddressByUserId(Long userId) {
		
		return addressRepository.findProfileAddressByUserId(userId); 
	}

	@Override
	public void updateProfileAddressByUserId(Map<String, String> userData, Long userId) {
		Address address = null;
		if(userId!=null) {
			address = addressRepository.findProfileAddressByUserId(userId) ;			
		}
		if(address!=null)
		  this.addOrUpdateAddress(  userData, address);
		
	}
	
	
}
