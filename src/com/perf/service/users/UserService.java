package com.perf.service.users;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import com.perf.base.exception.BaseException;
import com.perf.model.users.User;

public interface UserService {

	int deleteUser(Long uid);

	Optional<User> getUser(Long uid); 

	Long addUser(Map userData);

	User registerUser(Map<String, String> requestMap) throws BaseException;

	List<User> findAll();

	User logon(String logonId, String password);

	Collection<? extends GrantedAuthority> getGrantedAuthorities(Long userId);

	Collection<? extends GrantedAuthority> getGrantedAuthorities(User user);

	List<GrantedAuthority> getDefaultAuthority();

	User updateUser(Map<String, String> requestMap, Long userId) throws BaseException;

}