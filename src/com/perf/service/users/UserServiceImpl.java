package com.perf.service.users;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.perf.base.exception.BaseException;
import com.perf.constant.SecurityConstants;
import com.perf.model.security.Role;
import com.perf.model.users.User;
import com.perf.repository.security.RoleRepository;
import com.perf.repository.users.UserRepository;
import com.perf.security.rest.MyUserPrincipal;
import com.perf.service.security.GrantedAuthorityService;
import com.perf.utils.RequestHelper;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	GrantedAuthorityService grantedAuthorityService;
	
	@Autowired
	AddressService addressService;
	
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder userPasswordEncoder;

	@Override
	public Long addUser(Map userData) {
		User ue = new User();
		ue.setLogonId((String) userData.get("logonId"));
		ue.setPassword((String) userData.get("password"));
		userRepository.save(ue);
		return ue.getUserId();
	}

	@Override
	public Optional<User> getUser(Long uid) {

		return userRepository.findById( uid );

	}

	@Override
	public int deleteUser(Long uid) {
		try {
			userRepository.deleteById( uid );
		} catch (Exception e) {
			return -1;
		}
		return 0;
	}

	

	private void setAuthentication(User user, Collection<? extends GrantedAuthority> authorities) {

		UserDetails userDetail = new MyUserPrincipal(user, authorities);
		Authentication authentication = new UsernamePasswordAuthenticationToken(userDetail, null, authorities);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	@Override
	// TODO: configure default authority through some configuration
	public List<GrantedAuthority> getDefaultAuthority() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(SecurityConstants.ROLE_REGISTERED_USER));
		return authorities;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(User user) {
		return grantedAuthorityService.getGrantedAuthorities(user.getRoles());
		
		
	}
//	@Cacheable(  value="userCache")
	@Override
	public List<User> findAll() {

		System.out.println("find-all");
		return userRepository.findAll();
	}

	@Override
	public User logon(String logonId, String rawPassword) {
		Optional<User> ue = userRepository.findByLogonId(logonId);
		if (ue.isPresent()) {
			User user = ue.get();
			String encodedPassword = user.getPassword();
			// entry the input pwd to match
			if (userPasswordEncoder.matches(rawPassword, encodedPassword)) {

				Collection<? extends GrantedAuthority> authorities = grantedAuthorityService
						.getGrantedAuthorities(user.getUserId());
				this.setAuthentication(user, authorities);

				return user;

			}

		}
		return null;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(Long userId){
		return grantedAuthorityService.getGrantedAuthorities(userId);
	}
	
	public User registerUser(Map<String, String> userData) throws BaseException {
		String logonId = userData.get("logonId");
		Optional<User> ue = userRepository.findByLogonId(logonId);
		if (!ue.isPresent()) {

			 
		 
			User user = new User(); 
			userData.put("isProfile", "true");
			setUser(userData, user, true);
			addressService.addAddress(userData,user.getUserId());
			
			
			
			List<GrantedAuthority> authorities = getDefaultAuthority();
			setAuthentication(user, authorities);
			
			
			
			
			
			
			return user;
		}
		throw new BaseException(this.getClass().getName(),"regiseterUser","error.user.register.account.existed");
	}

	@Override
	public User updateUser(Map<String, String> userData, Long userId) throws BaseException{
		User user = userRepository.findById(userId).orElse(null);
		if(user==null) {
			throw new BaseException(this.getClass().getName(),"updateUser", "error.user.register.account.not.existed");
		}
		
		 
		setUser(userData, user,false);
		addressService.updateProfileAddressByUserId(userData,user.getUserId()); 
		return user;
	}

	private void setUser(Map<String, String> userData, User user, boolean isNewUser) throws BaseException {
		RequestHelper requestHelper = new RequestHelper(userData);
		user.setLogonId(requestHelper.getString("logonId"));
		
		String pwd = userData.get("password");
//		String pwd2 = userData.get("password2");
		
		if(isNewUser && (pwd==null  || pwd.isEmpty() ) ) {
			throw new BaseException(this.getClass().getName(),"registerUser","error.user.register.password.not.match");
		}
		// allow update user not reset pwd
		if(isNewUser || pwd!=null) {
			String encodedPwd = userPasswordEncoder.encode(pwd);
			user.setPassword(encodedPwd);
		}
		
		if(isNewUser) {
			Role role = roleRepository.findByName(SecurityConstants.ROLE_REGISTERED_USER) ;
			user.setRoles(Collections.singletonList(role));
		}
		userRepository.save(user);
	}

}
