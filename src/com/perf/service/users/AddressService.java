package com.perf.service.users;

import java.util.List;
import java.util.Map;

import com.perf.model.users.Address;

public interface AddressService {

	Address addAddress(Map addressData, Long userId);

	  

	Address findById(Long addressId);

	Address findByUserIdAndNickName(Long userId, String nickName);

	Address findProfileAddressByUserId(Long userId);

	boolean deleteAddressById(Long addressId);

	Address updateAddress(Map addressData, Long addressId);



	List<Address> findAllByUserId(Long userId);



	void updateProfileAddressByUserId(Map<String, String> userData, Long userId);

}