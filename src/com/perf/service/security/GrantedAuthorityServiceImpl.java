package com.perf.service.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import com.perf.model.security.Privilege;
import com.perf.model.security.Role;
import com.perf.model.users.User;
import com.perf.repository.users.UserRepository;

@Service
public class GrantedAuthorityServiceImpl implements GrantedAuthorityService {
	@Autowired
	UserRepository userRepository;

	@Override
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(Long userId) {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent()) {
			User userEntity = user.get();
			Collection<Role> roles = userEntity.getRoles();
			return getGrantedAuthorities(roles);

		}
		return null;
	}

	@Override
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(Collection<Role> roles) {
		List<GrantedAuthority> authorities
	      = new ArrayList<>();
		for (Role role: roles) {
	        authorities.add(new SimpleGrantedAuthority(role.getName()));
	        role.getPrivileges().stream()
	         .map(p -> new SimpleGrantedAuthority(p.getName()))
	         .forEach(authorities::add);
	    }
		return authorities;
//		return getGrantedAuthorities(getPrivileges(roles));
	}

//	private List<String> getPrivileges(Collection<Role> roles) {
//
//		List<String> privileges = new ArrayList<>();
//		List<Privilege> collection = new ArrayList<>();
//		for (Role role : roles) {
//			collection.addAll(role.getPrivileges());
//		}
//		for (Privilege item : collection) {
//			privileges.add(item.getName());
//		}
//		
//		
//	     
//		
//		
//		
//		return privileges;
//	}
//
//	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
//		List<GrantedAuthority> authorities = new ArrayList<>();
//		for (String privilege : privileges) {
//			authorities.add(new SimpleGrantedAuthority(privilege));
//		}
//		return authorities;
//	}

}
