package com.perf.service.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import com.perf.model.security.Role;

public interface GrantedAuthorityService {
	
	Collection<? extends GrantedAuthority> getGrantedAuthorities(Long userId);

	Collection<? extends GrantedAuthority> getGrantedAuthorities(Collection<Role> roles);

}